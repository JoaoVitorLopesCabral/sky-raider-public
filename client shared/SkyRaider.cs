﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using SkyRaider.Game;

namespace SkyRaider
{
    /// <summary>
    /// SkyRaider game!
    /// </summary>
    public class SkyRaider : Microsoft.Xna.Framework.Game
    {
        internal GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        #region Game World
        // Game world
        internal GameWorld World { get; set; }
        #endregion

        #region Resources - Non Game related
        /// <summary>
        /// Texture resource for menus
        /// </summary>
        internal Dictionary<string, Texture2D> TextureResources { get; set; }
        /// <summary>
        /// Sound effects for menus and others
        /// </summary>
        internal Dictionary<string, SoundEffect> SoundResources { get; set; }
        #endregion

        SpriteFont font;
        double lastUpdate;
        // rendertarget
        RenderTarget2D renderTarget;

        #region Menu
        /// <summary>
        /// Main game menu
        /// </summary>
        Menu MainMenu { get; set; }
        #endregion

        public SkyRaider()
        {
            graphics = new GraphicsDeviceManager( this );
            Content.RootDirectory = "Content";
            // Window Text
            Window.Title = "Sky Raider";
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 800;
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            renderTarget = new RenderTarget2D( GraphicsDevice, 1280, 800 );

            // initialize resource list
            TextureResources = new Dictionary<string, Texture2D>();
            SoundResources = new Dictionary<string, SoundEffect>();

            // Initialize random instance
            Util.Random = new Random();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch( GraphicsDevice ); 

            // load resources
            TextureResources.Add( "menu_newgame", Content.Load<Texture2D>( "menu_newgame" ) );
            TextureResources.Add( "menu_newgame_selected", Content.Load<Texture2D>( "menu_newgame_selected" ) );
            TextureResources.Add( "menu_options", Content.Load<Texture2D>( "menu_options" ) );
            TextureResources.Add( "menu_options_selected", Content.Load<Texture2D>( "menu_options_selected" ) );
            TextureResources.Add( "menu_exit", Content.Load<Texture2D>( "menu_exit" ) );
            TextureResources.Add( "menu_exit_selected", Content.Load<Texture2D>( "menu_exit_selected" ) );
            TextureResources.Add( "menu_resume", Content.Load<Texture2D>( "menu_resume" ) );
            TextureResources.Add( "menu_resume_selected", Content.Load<Texture2D>( "menu_resume_selected" ) );
            TextureResources.Add( "menu_easy", Content.Load<Texture2D>( "menu_easy" ) );
            TextureResources.Add( "menu_easy_selected", Content.Load<Texture2D>( "menu_easy_selected" ) );
            TextureResources.Add( "menu_normal", Content.Load<Texture2D>( "menu_normal" ) );
            TextureResources.Add( "menu_normal_selected", Content.Load<Texture2D>( "menu_normal_selected" ) );
            TextureResources.Add( "menu_hard", Content.Load<Texture2D>( "menu_hard" ) );
            TextureResources.Add( "menu_hard_selected", Content.Load<Texture2D>( "menu_hard_selected" ) );
            TextureResources.Add( "menu_resolution", Content.Load<Texture2D>( "menu_resolution" ) );
            TextureResources.Add( "menu_resolution_selected", Content.Load<Texture2D>( "menu_resolution_selected" ) );

            // Menu Arrows
            TextureResources.Add( "menu_arrow_left", Content.Load<Texture2D>( "menu_left" ) );
            TextureResources.Add( "menu_arrow_right", Content.Load<Texture2D>( "menu_right" ) );

            // TODO: use this.Content to load your game content here
            font = Content.Load<SpriteFont>( "Font" );
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update( GameTime gameTime )
        {
            // Allows the game to exit
            if ( GamePad.GetState( PlayerIndex.One ).Buttons.Back == ButtonState.Pressed )
                this.Exit();

            // TODO: Add your update logic here
            KeyboardState keypad = Keyboard.GetState();

            // if menu is null, create it
            if ( MainMenu == null )
                MainMenu = new Menu( this );

            #if WINDOWS
            // show menu if player pressed a key
            if ( keypad.IsKeyDown( Keys.Escape ) )
            {
                // check if the game is over
                if ( World != null && World.Rules.State == GameState.GameOver )
                    World = null; // delete world
                MainMenu.IsVisible = true;
            }
            #endif

            #if XBOX
            // show menu if player pressed select
            GamePadState gpad = GamePad.GetState( PlayerIndex.One );

            if ( gpad.IsButtonDown( Buttons.Start ) )
            {
                // check if the game is over
                if ( World != null && World.Rules.State == GameState.GameOver )
                    World = null; // delete world
                MainMenu.IsVisible = true;
            }
            #endif

            // update menu if it's not hidden
            if ( MainMenu.IsVisible )
            {
                // if game is running, pause
                if ( World != null )
                    World.Rules.SetState( GameState.OnMenu );

                MainMenu.Update( gameTime );
            }
            /*else
            {
                if ( World != null )
                {
                    if ( World.Rules.State == GameState.Waiting )
                        World.Precache( Content );
                    else
                        World.Update( gameTime );
                }

            }*/
            // before implementing the mainmenu
            if ( World == null )
            {
                // wait for start command
                if ( keypad.IsKeyDown( Keys.F1 ) )
                {
                    // create a new gameworld
                    World = new GameWorld( new SPGameRules( Game.GameDifficulty.Easy ), graphics );
                }
                else if ( keypad.IsKeyDown( Keys.F2 ) )
                {
                    World = new GameWorld( new SPGameRules( Game.GameDifficulty.Normal ), graphics );
                }
                else if ( keypad.IsKeyDown( Keys.F3 ) )
                {
                    World = new GameWorld( new SPGameRules( Game.GameDifficulty.Hard ), graphics );
                    lastUpdate = gameTime.TotalGameTime.TotalSeconds;
                }

                if ( World != null )
                {
                    // load game data
                    World.Precache( Content );
                    // set state
                    World.Rules.SetState( GameState.Paused );
                }
            }
            else
            {
                // If game is over, go back to main menu
                if ( World.Rules.State == GameState.GameOver )
                {
                    //World = null;
                    // set main menu as visible
                    //MainMenu.IsVisible = true;
                    //return;
                    return;
                }
                if ( keypad.IsKeyDown( Keys.F3 ) && ( gameTime.TotalGameTime.TotalSeconds - lastUpdate > 2.0f ) )
                {
                    if ( World.Players[ 0 ].dFlags == DebugFlags.GOD )
                        World.Players[ 0 ].dFlags = DebugFlags.NONE;
                    else
                        World.Players[ 0 ].dFlags = DebugFlags.GOD;

                    lastUpdate = gameTime.TotalGameTime.TotalSeconds;
                }
                if ( keypad.IsKeyDown( Keys.F4 ) )
                {
                    // clear player list
                    World = null;
                    // create a new gameworld
                    World = new GameWorld( new SPGameRules(Game.GameDifficulty.Normal), graphics );
                    // load game data
                    World.Precache( Content );
                    // set state
                    World.Rules.SetState( GameState.Paused );
                }

                World.Update( gameTime );
            }

            base.Update( gameTime );
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw( GameTime gameTime )
        {
            GraphicsDevice.SetRenderTarget( renderTarget );
            GraphicsDevice.Clear( Color.CornflowerBlue );

            spriteBatch.Begin();

            // check if the game is running
            if ( World != null )
            {
                World.Draw( spriteBatch );

                // menu comes last
                if ( MainMenu.IsVisible )
                {
                    MainMenu.Draw( spriteBatch );
                }
            }
            else
            {
                MainMenu.Draw( spriteBatch );
                // measure string
                /*float x = ( graphics.PreferredBackBufferWidth / 2 ) - ( font.MeasureString( "Press F1 to play on Easy Mode!\nPress F2 to play on Normal Mode!\nPress F3 to play on Hard Mode!" ).X / 2 );
                float y = ( graphics.PreferredBackBufferHeight / 2 ) - ( font.MeasureString( "Press F1 to play on Easy Mode!\nPress F2 to play on Normal Mode!\nPress F3 to play on Hard Mode!" ).Y / 2 );
                spriteBatch.DrawString( font, "Press F1 to play on Easy Mode!\nPress F2 to play on Normal Mode!\nPress F3 to play on Hard Mode!", new Vector2( x, y ), Color.Yellow );
            */}

            spriteBatch.End();
            
            GraphicsDevice.SetRenderTarget( null );
            
            GraphicsDevice.Clear( new Color( 47, 90, 160 ) );

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            spriteBatch.Draw( renderTarget, new Rectangle( 0, 0, 1280, 800 ), Color.White );

            spriteBatch.End();

            base.Draw( gameTime );
        }
    }
}
