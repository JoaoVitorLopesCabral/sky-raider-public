﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using SkyRaider.Game;

namespace SkyRaider
{
    /// <summary>
    /// Menu-like structure
    /// </summary>
    public class Menu
    {
        #region Properties
        /// <summary>
        /// Menu Items
        /// </summary>
        public List<MenuItem> Items { get; set; }
        /// <summary>
        /// Font
        /// </summary>
        protected SpriteFont Font { get; set; }
        /// <summary>
        /// Current selected menu item
        /// </summary>
        protected MenuItem SelectedItem { get; set; }
        /// <summary>
        /// Menu Texture Resources
        /// </summary>
        protected Dictionary<string, Texture2D> TextureResources { get; set; }
        /// <summary>
        /// Menu Sound Resources
        /// </summary>
        protected Dictionary<string, SoundEffect> SoundResources { get; set; }
        /// <summary>
        /// private (selected index)
        /// </summary>
        protected int _selectedIndex;
        /// <summary>
        /// Selected Index
        /// </summary>
        protected int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                // check if the index value is within bounds
                if ( value >= Items.Count || value < 0 )
                    return;
                // set value
                _selectedIndex = value;

                // set new menu item
                SelectedItem = Items[ value ];

                // mark item as selected
                SelectedItem.IsSelected = true;

                // unselect others
                for ( int i = 0; i < Items.Count; i++ )
                {
                    if ( i == value )
                        continue;

                    Items[ i ].IsSelected = false;
                }
            }
        }
        /// <summary>
        /// Last time an item was selected
        /// </summary>
        protected double SelectionTime { get; set; }
        /// <summary>
        /// Last time an item was pressed
        /// </summary>
        protected internal double PressedTime { get; set; }
        /// <summary>
        /// Game instance
        /// </summary>
        protected SkyRaider Game { get; set; }
        /// <summary>
        /// Menu visibility
        /// If hidden, do not render or update
        /// </summary>
        public bool IsVisible { get; set; }
        /// <summary>
        /// Sub menu visibility
        /// </summary>
        public bool IsOnSubMenu { get; set; }
        #endregion

        #region Positioning
        /// <summary>
        /// Menu Position
        /// </summary>
        protected Vector2 Position
        {
            get
            {
                // center
                float dx = ( Game.graphics.PreferredBackBufferWidth / 2 - 160 / 2 );
                float dy = ( Game.graphics.PreferredBackBufferHeight / 2 - ( Items.Count * 40 ) / 2 );

                return new Vector2( dx, dy );
            }
        }
        #endregion

        #region Functions
        /// <summary>
        /// Measure all Items Text property and returns the biggest
        /// </summary>
        /// <returns></returns>
        float GetLongestItemTextLength()
        {
            float highval = 0;

            // browse items
            for ( int i = 0; i < Items.Count; i++ )
            {
                if ( Font.MeasureString( Items[ i ].Text ).X > highval )
                    highval = Font.MeasureString( Items[ i ].Text ).X;
            }

            return highval;
        }
        /// <summary>
        /// Brings up the menu during gameplay
        /// </summary>
        public void Show()
        {
            // pause game
            Game.World.Rules.SetState( GameState.OnMenu );

            // set visibility to true
            IsVisible = true;
        }

        /// <summary>
        /// Hides the menu and resume game
        /// </summary>
        public void Hide()
        {
            // hide
            IsVisible = false;

            // if game was running, resume
            if ( Game.World != null )
            {
                // set game as paused
                Game.World.Rules.SetState( GameState.Paused );
            }
        }
        /// <summary>
        /// Load resources
        /// </summary>
        /// <param name="Content"></param>
        public void LoadResources( ContentManager Content )
        {
            // Textures
            TextureResources.Add( "menu_background", Content.Load<Texture2D>( "menu_panel" ) );
            TextureResources.Add( "item_selected", Content.Load<Texture2D>( "menu_item_selected" ) );
            TextureResources.Add( "menu_newgame", Content.Load<Texture2D>( "menu_newgame" ) );
            TextureResources.Add( "menu_selection_pointer", Content.Load<Texture2D>( "menu_selected_pos" ) );

            // Font
            Font = Content.Load<SpriteFont>( "Font" );
        }
        /// <summary>
        /// Control input/update logic for menus
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update( GameTime gameTime )
        {
            // if on sub menu, redirect call
            if ( IsOnSubMenu )
            {
                SelectedItem.Update( gameTime );
                return;
            }
            #if WINDOWS
            // get keyboard state
            KeyboardState kpad = Keyboard.GetState();
            // only move up/down after 0.25 seconds
            if ( gameTime.TotalGameTime.TotalSeconds - SelectionTime >= 0.25 )
            {
                // if the player presses up/down change the selected item
                if ( kpad.IsKeyDown( Keys.Up ) )
                {
                    SelectedIndex--;
                    SelectionTime = gameTime.TotalGameTime.TotalSeconds;
                }
                if ( kpad.IsKeyDown( Keys.Down ) )
                {
                    SelectedIndex++;
                    SelectionTime = gameTime.TotalGameTime.TotalSeconds;
                }
                // if current selected sub item has its own set of items
                if ( IsOnSubMenu && Items[ SelectedIndex ].Items.Count > 0 )
                {
                    if ( gameTime.TotalGameTime.TotalSeconds - SelectionTime >= 0.25 )
                    {
                        if ( kpad.IsKeyDown( Keys.Left ) )
                        {

                        }
                    }
                }
            }
            // if enter is pressed, get action from menu item
            if ( kpad.IsKeyDown( Keys.Enter ) &&  gameTime.TotalGameTime.TotalSeconds - PressedTime >= 0.50 )
            {
                // try raising the MenuItemPressed event
                SelectedItem.RaiseOnPressed();
                // set time
                PressedTime = gameTime.TotalGameTime.TotalSeconds;
            }
            #endif

            #if XBOX
            // get gamepad state
            GamePadState gpad = GamePad.GetState( PlayerIndex.One ); // TODO: Add support for local multiplayer

            // if the player presses up/down change the selected item
            if ( gpad.IsButtonDown( Buttons.DPadUp ) || gpad.IsButtonDown( Buttons.LeftThumbstickUp ) )
            {
                SelectedIndex--;
            }
            if ( gpad.IsButtonDown( Buttons.DPadDown ) || gpad.IsButtonDown( Buttons.LeftThumbstickDown ) )
            {
                SelectedIndex++;
            }

            // if start is pressed, get the menu action
            if ( gpad.IsButtonDown( Buttons.Start ) && gameTime.TotalGameTime.TotalSeconds - PressedTime >= 0.50 )
            {
                // try raising event
                SelectedItem.RaiseOnPressed();
                // set time
                PressedTime = gameTime.TotalGameTime.TotalSeconds;
            }
            #endif
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void Draw( SpriteBatch spriteBatch )
        {
            // if on sub menu, draw items
            if ( IsOnSubMenu )
            {
                for ( int i = 0; i < SelectedItem.Items.Count; i++ )
                {
                    // get text position
                    float py = Position.Y + ( i * 40 );
                    Vector2 ItemPos = new Vector2( Position.X, py );

                    if ( SelectedItem.Items[ i ].IsSelected )
                    {
                        spriteBatch.Draw( SelectedItem.Items[ i ].SelectedImage, ItemPos, Color.White );
                    }
                    else
                    {
                        spriteBatch.Draw( SelectedItem.Items[ i ].Image, ItemPos, Color.White );
                    }
                }
            }
            else
            {
                // draw items
                for ( int i = 0; i < Items.Count; i++ )
                {
                    // get text position
                    float py = Position.Y + ( i * 40 );
                    Vector2 ItemPos = new Vector2( Position.X, py );
                    // is item selected?
                    if ( Items[ i ].IsSelected )
                    {
                        spriteBatch.Draw( Items[ i ].SelectedImage, ItemPos, Color.White );
                    }
                    else
                    {
                        spriteBatch.Draw( Items[ i ].Image, ItemPos, Color.White );
                    }

                }
            }
        }
        #endregion

        #region Menu Events
        /// <summary>
        /// Exit button
        /// </summary>
        /// <param name="e"></param>
        void exit_OnPressed( MenuItemPressedEventArgs e )
        {
            Game.Exit();
        }
        /// <summary>
        /// New Game Button
        /// </summary>
        /// <param name="e"></param>
        void newgame_OnPressed( MenuItemPressedEventArgs e )
        {
            // show sub menu
            IsOnSubMenu = true;
        }
        /// <summary>
        /// New game on Hard difficulty
        /// </summary>
        /// <param name="e"></param>
        void mi_hard_OnPressed( MenuItemPressedEventArgs e )
        {
            //  Create game world
            Game.World = new GameWorld( new SPGameRules( global::SkyRaider.Game.GameDifficulty.Hard ), Game.graphics );

            // precache
            Game.World.Precache( Game.Content );

            // Hides menu (and set back to main menu)
            IsOnSubMenu = false;
            Hide();
        }
        /// <summary>
        /// New game on Normal difficulty
        /// </summary>
        /// <param name="e"></param>
        void mi_normal_OnPressed( MenuItemPressedEventArgs e )
        {
            //  Create game world
            Game.World = new GameWorld( new SPGameRules( global::SkyRaider.Game.GameDifficulty.Normal ), Game.graphics );

            // precache
            Game.World.Precache( Game.Content );

            // Hides menu (and set back to main menu)
            IsOnSubMenu = false;
            Hide();
        }
        /// <summary>
        /// New game on Easy difficulty
        /// </summary>
        /// <param name="e"></param>
        void mi_easy_OnPressed( MenuItemPressedEventArgs e )
        {
            //  Create game world
            Game.World = new GameWorld( new SPGameRules( global::SkyRaider.Game.GameDifficulty.Easy ), Game.graphics );

            // precache
            Game.World.Precache( Game.Content );

            // Hides menu (and set back to main menu)
            IsOnSubMenu = false;
            Hide();
        }
        /// <summary>
        /// Options
        /// </summary>
        /// <param name="e"></param>
        void options_OnPressed( MenuItemPressedEventArgs e )
        {
            IsOnSubMenu = true;
        }
        /// <summary>
        /// Resume game
        /// </summary>
        /// <param name="e"></param>
        void resume_OnPressed( MenuItemPressedEventArgs e )
        {
            if ( Game.World == null )
                return;
            // hide
            this.Hide();

            // resume
            Game.World.Rules.SetState( GameState.Paused );
        }
        #endregion

        #region Constructor
        /// <summary>
        /// New Menu (Empty)
        /// </summary>
        public Menu()
        {
            // Menu Items
            Items = new List<MenuItem>();
            // Textures
            TextureResources = new Dictionary<string, Texture2D>();
            // Sounds
            SoundResources = new Dictionary<string, SoundEffect>();
        }
        /// <summary>
        /// Create a new Menu Instance
        /// </summary>
        /// <param name="game"></param>
        public Menu( SkyRaider Game )
        {
            // Menu Items
            Items = new List<MenuItem>();
            // Textures
            TextureResources = new Dictionary<string, Texture2D>();
            // Sounds
            SoundResources = new Dictionary<string, SoundEffect>();
            // game
            this.Game = Game;

            // default visibility
            IsVisible = true;

            // Create Menu Items
            MenuItem newgame = new MenuItem( Game.TextureResources[ "menu_newgame" ], Game.TextureResources[ "menu_newgame_selected" ], this );
            newgame.OnPressed += new MenuItem.MenuItemPressedEventHandler( newgame_OnPressed );

            // Add child menu
            MenuItem mi_easy = new MenuItem( Game.TextureResources[ "menu_easy" ], Game.TextureResources[ "menu_easy_selected" ] );
            mi_easy.OnPressed += new MenuItem.MenuItemPressedEventHandler( mi_easy_OnPressed );
            newgame.Items.Add( mi_easy  );
            MenuItem mi_normal = new MenuItem( Game.TextureResources[ "menu_normal" ], Game.TextureResources[ "menu_normal_selected" ] );
            mi_normal.OnPressed += new MenuItem.MenuItemPressedEventHandler( mi_normal_OnPressed );
            newgame.Items.Add( mi_normal );
            MenuItem mi_hard = new MenuItem( Game.TextureResources[ "menu_hard" ], Game.TextureResources[ "menu_hard_selected" ] );
            mi_hard.OnPressed += new MenuItem.MenuItemPressedEventHandler( mi_hard_OnPressed );
            newgame.Items.Add( mi_hard );

            newgame.SelectedIndex = 0;

            Items.Add( newgame );

            // Add resume button
            MenuItem resume = new MenuItem( Game.TextureResources[ "menu_resume" ], Game.TextureResources[ "menu_resume_selected" ] );
            resume.OnPressed += new MenuItem.MenuItemPressedEventHandler( resume_OnPressed );
            Items.Add( resume );

            MenuItem options = new MenuItem( Game.TextureResources[ "menu_options" ], Game.TextureResources[ "menu_options_selected" ], this );
            options.OnPressed += new MenuItem.MenuItemPressedEventHandler( options_OnPressed );
            
            // add child
            MenuItem op_resolution = new MenuItem( Game.TextureResources[ "menu_resolution" ], Game.TextureResources[ "menu_resolution_selected" ] );
            op_resolution.OnPressed += new MenuItem.MenuItemPressedEventHandler( op_resolution_OnPressed );
            options.Items.Add( op_resolution );

            Items.Add( options );

            MenuItem exit = new MenuItem( Game.TextureResources[ "menu_exit" ], Game.TextureResources[ "menu_exit_selected" ] );
            exit.OnPressed += new MenuItem.MenuItemPressedEventHandler( exit_OnPressed );
            Items.Add( exit );

            // by default, select first item
            SelectedIndex = 0;
        }

        void op_resolution_OnPressed( MenuItemPressedEventArgs e )
        {
            
        }
        #endregion
    }
    /// <summary>
    /// Menu item (which can be another menu)
    /// </summary>
    public class MenuItem : Menu
    {
        #region Properties
        /// <summary>
        /// Gets/Sets if the current item is selected
        /// </summary>
        public bool IsSelected { get; set; }
        /// <summary>
        /// Text to be displayed
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Menu Image
        /// </summary>
        public Texture2D Image { get; set; }
        /// <summary>
        /// Image to display when this item is selected
        /// </summary>
        public Texture2D SelectedImage { get; set; }
        /// <summary>
        /// Owner menu
        /// </summary>
        private Menu Owner { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// MenuItemPressedEventHandle delegate
        /// </summary>
        public delegate void MenuItemPressedEventHandler( MenuItemPressedEventArgs e );
        /// <summary>
        /// OnPressed event (or when SPACE or button A is pressed while this menu item is selected)
        /// </summary>
        public event MenuItemPressedEventHandler OnPressed;
        #endregion

        #region Functions
        /// <summary>
        /// Raise the OnPressed Event
        /// </summary>
        /// <param name="e"></param>
        internal void RaiseOnPressed()
        {
            if ( OnPressed != null )
                OnPressed( new MenuItemPressedEventArgs( base.Game ) );
        }
        /// <summary>
        /// Update sub items selection
        /// </summary>
        /// <param name="gameTime"></param>
        new public void Update( GameTime gameTime )
        {
            #if WINDOWS
            // get keyboard state
            KeyboardState kpad = Keyboard.GetState();
            // only move up/down after 0.25 seconds
            if ( gameTime.TotalGameTime.TotalSeconds - SelectionTime >= 0.25 )
            {
                // if the player presses up/down change the selected item
                if ( kpad.IsKeyDown( Keys.Up ) )
                {
                    SelectedIndex--;
                    SelectionTime = gameTime.TotalGameTime.TotalSeconds;
                }
                if ( kpad.IsKeyDown( Keys.Down ) )
                {
                    SelectedIndex++;
                    SelectionTime = gameTime.TotalGameTime.TotalSeconds;
                }
            }
            // if enter is pressed, get action from menu item
            if ( kpad.IsKeyDown( Keys.Enter ) && gameTime.TotalGameTime.TotalSeconds - Owner.PressedTime >= 0.50 )
            {
                // try raising the MenuItemPressed event
                SelectedItem.RaiseOnPressed();

                Owner.PressedTime = gameTime.TotalGameTime.TotalSeconds;
            }
            // if user press ESC, go back to main menu
            if ( kpad.IsKeyDown( Keys.Escape ) )
                Owner.IsOnSubMenu = false;
            #endif

            #if XBOX
            // get gamepad state
            GamePadState gpad = GamePad.GetState( PlayerIndex.One ); // TODO: Add support for local multiplayer

            // if the player presses up/down change the selected item
            if ( gpad.IsButtonDown( Buttons.DPadUp ) || gpad.IsButtonDown( Buttons.LeftThumbstickUp ) )
            {
                SelectedIndex--;
            }
            if ( gpad.IsButtonDown( Buttons.DPadDown ) || gpad.IsButtonDown( Buttons.LeftThumbstickDown ) )
            {
                SelectedIndex++;
            }

            // if start is pressed, get the menu action
            if ( gpad.IsButtonDown( Buttons.Start ) && gameTime.TotalGameTime.TotalSeconds - Owner.PressedTime >= 0.50 )
            {
                // try raising event
                SelectedItem.RaiseOnPressed();

                Owner.PressedTime = gameTime.TotalGameTime.TotalSeconds;
            }
            // if pressed back, go to main menu
            if ( gpad.IsButtonDown( Buttons.Back ) )
                Owner.IsOnSubMenu = false;
            #endif
        }
        #endregion

        #region Constructor
        /// <summary>
        /// New Menu item
        /// </summary>
        /// <param name="Text">Item text</param>
        public MenuItem( string Text )
        {
            // set text
            this.Text = Text;
        }
        /// <summary>
        /// New Menu Item
        /// </summary>
        /// <param name="Image">Item image</param>
        public MenuItem( Texture2D Image, Texture2D SelectedImage )
        {
            this.Image = Image;
            this.SelectedImage = SelectedImage;
        }
        /// <summary>
        /// New Menu Item
        /// </summary>
        /// <param name="Image">Item image</param>
        public MenuItem( Texture2D Image, Texture2D SelectedImage, Menu Owner )
        {
            this.Image = Image;
            this.SelectedImage = SelectedImage;
            this.Owner = Owner;
        }
        #endregion
    }
    /// <summary>
    /// Menu Item Pressed event args
    /// </summary>
    public class MenuItemPressedEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// Game data
        /// </summary>
        public SkyRaider Game { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new MenuItemPressedEventArgs
        /// </summary>
        /// <param name="Game">Game main class</param>
        public MenuItemPressedEventArgs( SkyRaider Game )
        {
            this.Game = Game;
        }
        #endregion
    }
}