﻿using System;

namespace SkyRaider
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (SkyRaider game = new SkyRaider())
            {
                game.Run();
            }
        }
    }
#endif
}

