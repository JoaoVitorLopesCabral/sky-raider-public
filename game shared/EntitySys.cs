﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Possible facing directions
    /// </summary>
    public enum FacingDirection { West, East };
    #if DEBUG
    /// <summary>
    /// Debug flags
    /// </summary>
    public enum DebugFlags { NONE, GOD };
    #endif
    /// <summary>
    /// Constains useful functions
    /// </summary>
    public static partial class Util
    {
        #region Entity Related
        /// <summary>
        /// Gets all entities within the range of the given unit
        /// </summary>
        /// <param name="refEnt">Reference entity</param>
        /// <returns></returns>
        public static Entity[] GetEntitiesInRange( CombatEntity refEnt )
        {
            // entity array
            List<Entity> found = new List<Entity>();
            // browse entites
            foreach ( Entity ent in refEnt.World.Entities )
            {
                if ( Vector2.Distance( ent.Position, refEnt.Position ) <= refEnt.InfluenceRange )
                    found.Add( ent );
            }
            // players
            foreach ( Entity ent in refEnt.World.Players )
            {
                if ( Vector2.Distance( ent.Position, refEnt.Position ) <= refEnt.InfluenceRange )
                    found.Add( ent );
            }
            return found.ToArray();
        }
        /// <summary>
        /// Get all entities with the given flag and are within range (100)
        /// </summary>
        /// <param name="Flag">Collision flag - filter</param>
        /// <returns></returns>
        public static Entity[] GetEntitiesByFlag( Entity refEnt, CollisionGroup Flag )
        {
            List<Entity> found = new List<Entity>();

            foreach ( Entity ent in refEnt.World.Entities )
            {
                if ( ent.Collision == Flag && Vector2.Distance(ent.Position, refEnt.Position) <= 100 )
                    found.Add( ent );
            }

            return found.ToArray();
        }
        #endregion
    }
}