﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Items that can be picked-up
    /// </summary>
    public enum ItemType
    {
        /// <summary>
        /// Power-up item
        /// </summary>
        PowerUp,
        /// <summary>
        /// Armor
        /// </summary>
        Equipment,
        /// <summary>
        /// New weapon
        /// </summary>
        Weapon,
        /// <summary>
        /// A random item
        /// </summary>
        Random,
    };
    /// <summary>
    /// Player weapons
    /// </summary>
    public enum WeaponType
    {
        /// <summary>
        /// Regular weapon
        /// </summary>
        Cannon,
        /// <summary>
        /// Dual-Fire cannon
        /// </summary>
        DoubleCannon,
        /// <summary>
        /// Bomb, can destroy all enemies in range
        /// </summary>
        Bomb,
        /// <summary>
        /// No weapons at all
        /// </summary>
        None,
    };
}