﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Represents a fuel deposit.
    /// Hover over it to refuel.
    /// </summary>
    public class FuelDepot : PowerUpEntity
    {
        #region Properties
        /// <summary>
        /// Time before sending the next barrel of fuel
        /// </summary>
        double RefuelTime { get; set; }
        /// <summary>
        /// % of fuel to give the player
        /// </summary>
        float RefuelEfficiency { get; set; }
        /// <summary>
        /// last refuel time
        /// </summary>
        double LastRefuel { get; set; }
        /// <summary>
        /// Player that activated this entity
        /// </summary>
        Player pActivator { get; set; }
        #endregion

        #region Functions
        /// <summary>
        /// Refuel logic
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update( GameTime gameTime )
        {
            if ( State == EntityState.Dead )
            {
                if ( deathAnimFrames >= DeathAnim.getFrameCount() )
                {
                    // entity dead and has finished running death animation, remove it
                    // explodes
                    Explosion blast = new Explosion( Origin, 1, 100, World );
                    blast.Trigger();
                    Kill();
                    return;
                }
                
                DeathAnimTime += gameTime.ElapsedGameTime.TotalMilliseconds;

                if ( DeathAnimTime >= DeathAnim.Speed )
                {
                    DeathAnim.SetNextFrame();
                    DeathAnimTime = 0;
                    // update frame count
                    deathAnimFrames++;
                }
            }

            // animation
            AnimTime += gameTime.ElapsedGameTime.TotalMilliseconds;

            if ( AnimTime >= Sprite.Speed )
            {
                Sprite.SetNextFrame();
                AnimTime = 0;
            }

            // move
            base.Update( gameTime );
            // check if the player is within bounds
            for ( int i = 0; i < World.Players.Count; i++ )
            {
                if ( GetBounds().Intersects( World.Players[ i ].GetBounds() ) )
                    pActivator = World.Players[ i ];
                else
                    pActivator = null;
            }
            // refuel
            if ( ( gameTime.TotalGameTime.TotalSeconds - LastRefuel ) > RefuelTime )
            {
                // check if the player is full
                if ( pActivator == null )
                    return;
                if ( pActivator.Fuel < 1 )
                {
                    if ( pActivator.Fuel + RefuelEfficiency > 1 )
                        pActivator.Fuel += ( 1 - pActivator.Fuel );
                    else
                        pActivator.Fuel += RefuelEfficiency;
                    // time
                    LastRefuel = gameTime.TotalGameTime.TotalSeconds;
                }
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new FuelDepot instance, with default settings
        /// </summary>
        public FuelDepot(GameWorld World, Vector2 Position)
        {
            // world
            this.World = World;
            // pos
            this.Position = Position;
            // refuel time
            RefuelTime = 0.1;
            // efficiency
            RefuelEfficiency = 0.075f;
            // sprite
            //Sprite = new Sprite( new List<Texture2D>() { World.TextureAssets[ "fuel_depot" ] } );
            Sprite = new Sprite( World.AnimationAssets[ "fuel_depot" ] );
            Sprite.Speed = 200.0f;
            // set death anim
            DeathAnim = new Sprite( World.AnimationAssets[ "explosion" ] );
            DeathAnim.Speed = 200.0f;
            // collision
            Collision = CollisionGroup.Special;
            // Health
            Health = 1;
            // Score
            ScoreValue = 50;
        }
        #endregion
    }
}