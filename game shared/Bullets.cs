﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Cannon Bullets, basic ammo
    /// </summary>
    public class BaseBullet : Entity
    {
        #region Properties
        /// <summary>
        /// Bullet travelling speed
        /// </summary>
        public float Speed { get; set; }
        /// <summary>
        /// Gets/Sets if this bullet should do splash damage
        /// </summary>
        public bool DoesSplashDamage { get; set; }
        /// <summary>
        /// Splash range
        /// </summary>
        public float SplashRange { get; set; }
        /// <summary>
        /// Bullet operational range
        /// </summary>
        public float Range { get; set; }
        /// <summary>
        /// Bullet target (not guiaded, just as ref)
        /// </summary>
        public Vector2 Target;
        /// <summary>
        /// Entity that fired it
        /// </summary>
        public CombatEntity Owner { get; set; }
        /// <summary>
        /// AI that owns this entity
        /// </summary>
        public CombatEntity AIOwner { get; set; }
        #endregion

        #region Functions
        /// <summary>
        /// If the bullet is hit by another bullet or explosion, simply remove it from the game.
        /// </summary>
        /// <param name="Attacker"></param>
        /// <param name="Damage"></param>
        public override void Hit( Entity Attacker, int Damage )
        {
            Kill();
        }
        /// <summary>
        /// Removes this entity
        /// </summary>
        public override void Kill()
        {
            Owner.Bullets.Remove( this );
        }
        /// <summary>
        /// Bullet update data
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update( GameTime gameTime )
        {
            // check for collisions
            foreach ( Entity cEnt in Owner.World.Entities )
            {
                // prevent bullet from hitting owner
                if ( cEnt == Owner )
                    continue;

                // check collision group
                if ( cEnt.Collision == CollisionGroup.NotSolid )
                    continue;
                // bounds 
                Rectangle bounds = cEnt.GetBounds();
                // check collision
                if ( bounds.Intersects( GetBounds() ) )
                {
                    if ( cEnt.State == EntityState.Invulnerable )
                        continue;
                    // hit
                    cEnt.Hit( this.Owner );
                    // remove itself
                    Kill();
                }
            }

            // AI
            // check for collisions
            foreach ( Entity cEnt in Owner.World.AIUnits )
            {
                // prevent bullet from hitting owner
                if ( cEnt == Owner )
                    continue;

                // check collision group
                if ( cEnt.Collision == CollisionGroup.NotSolid )
                    continue;
                // bounds 
                Rectangle bounds = cEnt.GetBounds();
                // check collision
                if ( bounds.Intersects( GetBounds() ) )
                {
                    if ( cEnt.State == EntityState.Invulnerable )
                        continue;
                    // hit
                    cEnt.Hit( this.Owner );
                    // remove itself
                    Owner.Bullets.Remove( this );
                }
            }
            // player collision
            foreach ( Entity player in Owner.World.Players )
            {
                // dont hit owner
                if ( player == Owner )
                    continue;

                // bounds 
                Rectangle bounds = new Rectangle( (int)player.Position.X, (int)player.Position.Y, player.Sprite.getFrame().Width, player.Sprite.getFrame().Height );
                // check collision
                if ( bounds.Contains( Util.ToPoint( Position ) ) )
                {
                    // hit
                    player.Hit( this.Owner );
                    // remove itself
                    Owner.Bullets.Remove( this );
                }
            }

            // check if we reach the target
            if ( Position == Target )
            {
                // remove itself from the owner list
                Kill();
            }

            // Are we there yet?
            if ( Position == Target )
                return; // modify this line if the bullet should explode or something

            // since the distance might vary for each axis, let's find a suitable speed to avoid making turns
            float dx = ( Target.X - Position.X );
            float dy = ( Target.Y - Position.Y );
            float dz = Vector2.Distance( Target, Position );
            // each axis has its speed based on the angle
            Position.X += Speed * (float)Math.Sin( MathHelper.ToRadians( dy / dz ) ) * -1;
            Position.Y += Speed * (float)Math.Cos( MathHelper.ToRadians( dx / dz ) ) * -1;


            // check if we're past target
            if ( Vector2.Distance( Position, Owner.Position ) > Range )
                Kill();
        }
        /// <summary>
        /// Drawing
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw( SpriteBatch spriteBatch )
        {
            base.Draw( spriteBatch );
        }
        /// <summary>
        /// Get entity collision bounds
        /// </summary>
        /// <returns></returns>
        public override Rectangle GetBounds()
        {
            return new Rectangle( (int)Position.X, (int)Position.Y,
                2, Sprite.getFrame().Height );
        }
        #endregion

        #region Constructor
        /// <summary>
        /// For easier class derivation
        /// </summary>
        public BaseBullet() { }
        /// <summary>
        /// New bullet data
        /// </summary>
        /// <param name="Owner">Entity that fired it.</param>
        public BaseBullet( CombatEntity Owner ) 
        {
            this.Position = Owner.Origin; this.Owner = Owner;
            // if the player is firing
            if ( Owner.IsPlayer )
                Target.Y = Owner.Position.Y - 200;
            else
                Target = Owner.Target.Origin;
            // SPRITE
            Sprite = new Sprite( new List<Texture2D>() { Owner.World.TextureAssets[ "player_bullet" ] } );
            // speed
            Speed = 5.0f;
            // range
            Range = Owner.World.ClientInfo.ScreenHeight;
        }
        #endregion
    }
    /// <summary>
    /// Bullets fired by enemy boats
    /// </summary>
    public class AACannonBullet : BaseBullet
    {
        #region Properties
        /// <summary>
        /// Bullet direction
        /// </summary>
        public Vector2 Direction;
        #endregion

        #region Functions
        /// <summary>
        /// Update logic
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update( GameTime gameTime )
        {
            // check for collisions
            foreach ( Entity cEnt in Owner.World.Entities )
            {
                // prevent bullet from hitting owner
                if ( cEnt == Owner )
                    continue;

                // check collision group
                if ( cEnt.Collision == CollisionGroup.NotSolid )
                    continue;
                // bounds 
                Rectangle bounds = cEnt.GetBounds();
                // check collision
                if ( bounds.Intersects( GetBounds() ) )
                {
                    // check for entity invulnerability
                    if ( cEnt.State == EntityState.Invulnerable )
                        continue;
                    // hit
                    cEnt.Hit( this.Owner );
                    // remove itself
                    Kill();
                }
            }
            // player collision
            foreach ( Entity player in Owner.World.Players )
            {
                // dont hit owner
                if ( player == Owner )
                    continue;

                // bounds 
                Rectangle bounds = new Rectangle( (int)player.Position.X, (int)player.Position.Y, player.Sprite.getFrame().Width, player.Sprite.getFrame().Height );
                // check collision
                if ( bounds.Contains( Util.ToPoint( Position ) ) )
                {
                    // hit
                    player.Hit( this.Owner );
                    // remove itself
                    Kill();
                }
            }
            // move
            Position += ( Direction * Speed );
            // check if owner is dead, and if we have reached the operational range
            if ( Owner.Health <= 0 && Vector2.Distance( SpawnPosition, Position ) > Range )
            {
                Kill();
            }
        }

        public override void Draw( SpriteBatch spriteBatch )
        {
            spriteBatch.Draw( Sprite.getFrame(), Position, null, Sprite.RenderInfo.TintingColor,
                Sprite.Rotation, Vector2.Zero, new Vector2( Sprite.Scale.X, 2 ), SpriteEffects.None, 0 );
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create a new AA Cannon Bullet
        /// </summary>
        /// <param name="Owner"></param>
        public AACannonBullet( CombatEntity Owner )
        {
            // start pos
            SpawnPosition = Owner.Origin;
            // Position
            this.Position = Owner.Origin;
            //this.Origin = Owner.Origin;
            // Owner
            this.Owner = Owner;
            // sprite
            Sprite = new Sprite( new List<Texture2D>() { Owner.World.TextureAssets[ "bullet_aa" ] } );
            // scale
            Sprite.Scale = new Vector2( 15.0f );
            // rotation
            Sprite.Rotation = (float)Owner.Angles.GetRelativeAngle();
            // Direction
            Direction = Owner.Angles.GetDirectionVector();
            // Speed
            Speed = 7.0f;
            // Range
            Range = 500.0f;
        }
        /// <summary>
        /// Creates a new AA Cannon bullet
        /// </summary>
        /// <param name="Owner">Entity that is firing it</param>
        /// <param name="Origin">Bullet origin</param>
        public AACannonBullet( CombatEntity Owner, Point Origin )
        {
            // start pos
            SpawnPosition = Util.ToVector2( Origin );
            // Position
            this.Position = Util.ToVector2( Origin );
            // Owner
            this.Owner = Owner;
            // sprite
            Sprite = new Sprite( new List<Texture2D>() { Owner.World.TextureAssets[ "bullet_aa" ] } );
            // scale
            Sprite.Scale = new Vector2( 15.0f );
            // rotation
            Sprite.Rotation = (float)Owner.Angles.GetRelativeAngle();
            // Direction
            Direction = Owner.Angles.GetDirectionVector();
            // Speed
            Speed = 7.0f;
            // Range
            Range = 500.0f;
        }
        #endregion
    }
}