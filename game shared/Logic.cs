﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Trigger entity, can be activated when another entity touches it
    /// </summary>
    public class BaseTrigger : Entity
    {
        #region Properties
        /// <summary>
        /// Trigger area
        /// </summary>
        public Rectangle TriggerArea;
        /// <summary>
        /// Filter entity, if set, only this entity can activate the trigger
        /// </summary>
        public Entity Filter;
        /// <summary>
        /// Target entity
        /// </summary>
        public Entity Target;
        #endregion

        #region Functions
        /// <summary>
        /// Detects if another entity is within the trigger area
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update( GameTime gameTime )
        {
            if ( Filter != null )
            {
                if ( TriggerArea.Contains( new Point( (int)Filter.Position.X, (int)Filter.Position.Y ) ) )
                    OnTrigger( Filter ); // Each type of trigger should override the Think function
            }

            // no filter, lets seach for all entities then and stop at the first collision
            foreach ( Entity ent in World.Entities )
            {
                if ( TriggerArea.Contains( new Point( (int)ent.Position.X, (int)ent.Position.Y ) ) )
                    OnTrigger( ent );
            }
        }
        /// <summary>
        /// For basic triggers, it will wake enemy units
        /// </summary>
        public virtual void OnTrigger(Entity Activator)
        {
            // try to convert to CombatEntity
            ( (CombatEntity)Target ).Wake( Activator );
        }
        #endregion

        #region Constructor
        public BaseTrigger() { }
        #endregion
    }
    /// <summary>
    /// Defines a player start point
    /// </summary>
    public class PlayerSpawnPoint : Entity
    {
        /// <summary>
        /// Invisible entity
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw( SpriteBatch spriteBatch )
        {
            // do not draw!
        }
        /// <summary>
        /// Creates a new entity at the given position;
        /// </summary>
        /// <param name="Position">Entity pos</param>
        public PlayerSpawnPoint( Vector2 Position, GameWorld World )
        {
            this.Position = Position;
            this.World = World;
            Collision = CollisionGroup.NotSolid;
        }
    }
    /// <summary>
    /// Respawn point for players
    /// </summary>
    public class RespawnPoint : Entity
    {
    }
    /// <summary>
    /// Respawn point for AI units
    /// </summary>
    public class UnitSpawnPoint : LogicalEntity
    {
        #region Properties
        /// <summary>
        /// Area to spawn units
        /// </summary>
        Rectangle SpawnArea { get; set; }
        /// <summary>
        /// Time between spawning units
        /// </summary>
        double SpawnTime { get; set; }
        /// <summary>
        /// Gets/Sets if spawning is enabled
        /// </summary>
        bool IsEnabled { get; set; }
        /// <summary>
        /// Fuel spawn time
        /// </summary>
        double FuelSpawnTime { get; set; }
        /// <summary>
        /// Last fuel spawn time
        /// </summary>
        double LastFuelSpawn { get; set; }
        #endregion

        #region Functions
        /// <summary>
        /// Generates a new position within the SpawnArea
        /// </summary>
        /// <returns></returns>
        Vector2 CreateUnitPosition()
        {
            float dX = Util.Random.Next( SpawnArea.Left, SpawnArea.Right );
            float dY = Util.Random.Next( SpawnArea.Top, SpawnArea.Bottom );

            return new Vector2( dX, dY );
        }
        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update( GameTime gameTime )
        {
            // should we spawn?
            if ( gameTime.TotalGameTime.TotalSeconds - LastUpdate >= SpawnTime )
            {
                // create position and spawn
                for ( int i = 0; i < World.EnemySpawnQuantity; i++ )
                {
                    // just a test
                    if ( Util.Random.Next( 0, 4 ) == 1 )
                    {
                        // spawn a turret
                        SentryTurret turret = new SentryTurret( World );
                        turret.SpawnPosition = CreateUnitPosition();
                        turret.Position = turret.SpawnPosition;

                        World.AIUnits.Add( turret );
                    }
                    else
                    {
                        // TODO: Pick different entities based on difficulty
                        Frigate frigate = new Frigate( World );
                        frigate.SpawnPosition = CreateUnitPosition();
                        frigate.Position = frigate.SpawnPosition;

                        World.AIUnits.Add( frigate );
                    }
                }

                LastUpdate = gameTime.TotalGameTime.TotalSeconds;
            }

            // fuel
            if ( gameTime.TotalGameTime.TotalSeconds - LastFuelSpawn >= FuelSpawnTime )
            {
                // spawn fuel 
                for ( int i = 0; i < World.FuelSpawnQuantity; i++ )
                {
                    // create position
                    FuelDepot fuel = new FuelDepot( World, CreateUnitPosition() );
                    World.Entities.Add( fuel );
                }

                LastFuelSpawn = gameTime.TotalGameTime.TotalSeconds;
            }
            
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Creates a new UnitSpawnPoint entity
        /// </summary>
        /// <param name="SpawnArea">Rectangle representing the spawn area</param>
        /// <param name="World">World to send the entities to</param>
        public UnitSpawnPoint( Rectangle SpawnArea, GameWorld World )
        {
            // Assign params
            this.SpawnArea = SpawnArea;
            this.World = World;

            // default spawning values
            SpawnTime = 5.0f;
            FuelSpawnTime = 3.0f;
        }
        #endregion
    }
}