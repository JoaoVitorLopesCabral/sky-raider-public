﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    public enum GameDifficulty
    {
        /// <summary>
        /// Easy mode
        /// More fuel less enemies
        /// </summary>
        Easy,
        /// <summary>
        /// Normal spawns
        /// </summary>
        Normal,
        /// <summary>
        /// Hard!
        /// More enemies less fuel
        /// </summary>
        Hard,
    }
    /// <summary>
    /// Possible game states 
    /// </summary>
    public enum GameState
    {
        /// <summary>
        /// Triggered upon player's death, if lives are less then 1, it will trigget GameOver state
        /// </summary>
        PlayerDeath,
        /// <summary>
        /// Game Over!
        /// </summary>
        GameOver,
        /// <summary>
        /// Game is running, so, nothing special here.
        /// </summary>
        GameRunning,
        /// <summary>
        /// This mode starts a game server, for multiplayer.
        /// </summary>
        Listening,
        /// <summary>
        /// A multiplayer match is running.
        /// </summary>
        MPMatch,
        /// <summary>
        /// Game Paused.
        /// </summary>
        Paused,
        /// <summary>
        /// Before game has started
        /// </summary>
        Waiting,
        /// <summary>
        /// Loading stuff, do not render anything
        /// </summary>
        Loading,
        /// <summary>
        /// Player is vewing a menu (pause game update)
        /// </summary>
        OnMenu,
    }
    /// <summary>
    /// Represents a set of rules for the game.
    /// </summary>
    public class GameRules
    {
        #region Properties
        /// <summary>
        /// Game difficulty Mode
        /// </summary>
        public GameDifficulty Difficulty { get; set; }
        /// <summary>
        /// Maximum number of players
        /// </summary>
        public int MaxPlayers { get; set; }
        /// <summary>
        /// Current game state
        /// </summary>
        public GameState State { get; private set; }
        /// <summary>
        /// Movement speed
        /// </summary>
        public float Speed { get; set; }
        /// <summary>
        /// Speed boost
        /// </summary>
        public float SpeedBoost { get; set; }
        /// <summary>
        /// Player current level
        /// </summary>
        public int CurrentLevel { get; set; }
        #endregion

        #region Scores
        /// <summary>
        /// Score multiplier
        /// </summary>
        public float ScoreModifier { get; set; }
        #endregion

        #region Entity Spawning
        /// <summary>
        /// Base amount of fuel depots to spawn (based on difficulty)
        /// </summary>
        public int BaseFuelQuantity { get; set; }
        /// <summary>
        /// Incremental factor per level [ 1 + ( level * factor ) ]
        /// </summary>
        public float FuelIncrementFactor { get; set; }
        /// <summary>
        /// Base amount of enemy entities
        /// </summary>
        public int BaseEnemyQuantity { get; set; }
        /// <summary>
        /// Incremental factor per level [ 1 + ( level * factor ) ]
        /// </summary>
        public float EnemyIncrementFactor { get; set; }
        /// <summary>
        /// Maximum amount of enemy entities that can be spawned
        /// </summary>
        public int MaxEnemyQuantity { get; set; }
        /// <summary>
        /// Reset enemy count every multiple of this value (boost enemy stats when it happens)
        /// </summary>
        public int LevelToResetEnemyQuantity { get; set; }
        /// <summary>
        /// Current enemy level
        /// </summary>
        public int EnemyLevel { get; set; }
        #endregion

        #region Functions
        /// <summary>
        /// Set a new game state
        /// </summary>
        /// <param name="State">New state</param>
        public void SetState( GameState State )
        {
            this.State = State;
        }
        /// <summary>
        /// Gets the total experience for the given level
        /// </summary>
        /// <param name="Level">Target level</param>
        /// <returns></returns>
        public int GetTotalExperience( int Level )
        {
            // This is a simple geometrical progression
            int A = 100; // level 2
            float q = 1.5f; // pg ratio

            #if XBOX
            int experencie = (int)Math.Round( ( A * ( Math.Pow( q, Level - 1 ) ) ), 0 );
            #else
            int experencie = (int)Math.Round( ( A * ( Math.Pow( q, Level - 1 ) ) ), MidpointRounding.AwayFromZero );
            #endif

            return experencie;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new GameRules instance
        /// </summary>
        public GameRules()
        {
            // default score multiplayer
            ScoreModifier = 1.0f;
            // max players
            MaxPlayers = 1;
            // set state
            State = GameState.Waiting;
            // speed
            Speed = 2.0f;
            // boost
            SpeedBoost = 1.0f;
            // Normal mode
            Difficulty = GameDifficulty.Normal;

            // Base values
            BaseEnemyQuantity = 3;
            BaseFuelQuantity = 4;

            FuelIncrementFactor = -5.0f;
            EnemyIncrementFactor = 2.5f;
        }

        public GameRules( GameDifficulty Difficulty )
        {
            // set difficulty
            this.Difficulty = Difficulty;
            
            // set state
            State = GameState.Waiting;

            // setup spawns and scores
            switch ( Difficulty )
            {
                case GameDifficulty.Easy:
                    ScoreModifier = 1.2f;
                    Speed = 2.0f;
                    SpeedBoost = 1.0f;
                    BaseFuelQuantity = 5;
                    BaseEnemyQuantity = 2;
                    FuelIncrementFactor = -1.0f;
                    EnemyIncrementFactor = 1.0f;
                    break;
                case GameDifficulty.Normal:
                    ScoreModifier = 1f;
                    Speed = 2.0f;
                    SpeedBoost = 1.0f;
                    BaseFuelQuantity = 4;
                    BaseEnemyQuantity = 3;
                    FuelIncrementFactor = -5.0f;
                    EnemyIncrementFactor = 2.5f;
                    break;
                case GameDifficulty.Hard:
                    ScoreModifier = 0.8f;
                    Speed = 2.0f;
                    SpeedBoost = 1.0f;
                    BaseFuelQuantity = 3;
                    BaseEnemyQuantity = 5;
                    FuelIncrementFactor = -7.0f;
                    EnemyIncrementFactor = 3.5f;
                    break;
            }
        }
        #endregion
    }
}