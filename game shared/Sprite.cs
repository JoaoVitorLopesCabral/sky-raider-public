﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Sprite types
    /// </summary>
    public enum SpriteTypes
    {
        /// <summary>
        /// Static sprite.
        /// </summary>
        Static,
        /// <summary>
        /// Animated sprite.
        /// </summary>
        Animated,
    }
    /// <summary>
    /// Represents a sprite
    /// </summary>
    public class Sprite
    {
        #region General Settings
        /// <summary>
        /// Sprite Type
        /// </summary>
        public SpriteTypes Type { get; set; }
        #endregion

        #region Assets and Animation
        /// <summary>
        /// Textures assigned to this sprite
        /// </summary>
        private List<Texture2D> _textures { get; set; }
        /// <summary>
        /// Get the current drawing frame
        /// </summary>
        /// <returns></returns>
        public Texture2D getFrame()
        {
            // if static, return index 0
            if ( Type == SpriteTypes.Static )
                return _textures[ 0 ];
            // as it is animated, return the current frame
            return _textures[ _frame ];
        }
        /// <summary>
        /// Get current frame index
        /// </summary>
        /// <returns></returns>
        public int getFrameIndex()
        {
            return _frame;
        }
        /// <summary>
        /// Returns the frame count
        /// </summary>
        /// <returns></returns>
        public int getFrameCount()
        {
            return _textures.Count;
        }
        /// <summary>
        /// Frame to be rendered
        /// </summary>
        private int _frame { get; set; }
        /// <summary>
        /// Set next animation frame
        /// </summary>
        public void SetNextFrame()
        {
            if ( _textures.Count == ( _frame + 1 ) )
                _frame = 0;
            else
                _frame++;
        }
        /// <summary>
        /// Set rendering frame
        /// </summary>
        /// <param name="Frame"></param>
        public void SetFrame( int Frame )
        {
            _frame = Frame;
        }
        /// <summary>
        /// Set rendering frame
        /// </summary>
        /// <param name="Frame">Texture2D object! If it doesn't exist in the sprite frame list, it'll be added</param>
        public void SetFrame( Texture2D Frame )
        {
            if ( !_textures.Contains( Frame ) )
                _textures.Add( Frame );

            _frame = _textures.IndexOf( Frame );
        }
        #endregion

        #region Appearance
        /// <summary>
        /// Rendering attributes
        /// </summary>
        public RenderProperties RenderInfo { get; set; }
        /// <summary>
        /// Texture scale
        /// </summary>
        public Vector2 Scale { get; set; }
        /// <summary>
        /// Texture rotation (rad)
        /// </summary>
        public float Rotation { get; set; }
        /// <summary>
        /// Animation speed (sec)
        /// </summary>
        public float Speed { get; set; }
        /// <summary>
        /// Initializes Appearance properties with the default value
        /// </summary>
        private void InitializeAppearance()
        {
            // Render
            RenderInfo = new RenderProperties();
            // Scale
            Scale = new Vector2( 1 );
            // Rotation
            Rotation = 0;
            // Speed
            Speed = 1;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// New sprite instance
        /// </summary>
        /// <param name="Textures">Textures (frames) that this sprite is composed of</param>
        public Sprite( List<Texture2D> Textures )
        {
            // Assign textures
            _textures = Textures;
            // identify if it is animated or not
            if ( _textures.Count > 1 ) // if > 1, assume animated
                Type = SpriteTypes.Animated;
            // Assume all other rendering attributes to 1
            InitializeAppearance();
        }
        /// <summary>
        /// New sprite instance
        /// </summary>
        /// <param name="Textures">Textures (frames) that this sprite is composed of</param>
        /// <param name="RenderInfo">Rendering Attributes</param>
        public Sprite( List<Texture2D> Textures, RenderProperties RenderInfo )
        {
            // Assign textures
            _textures = Textures;
            // identify if it is animated or not
            if ( _textures.Count > 1 ) // if > 1, assume animated
                Type = SpriteTypes.Animated;
            // render
            this.RenderInfo = RenderInfo;
            // All other equals default
            // Scale
            Scale = new Vector2( 1 );
            // Rotation
            Rotation = 0;
            // Speed
            Speed = 1;
        }
        /// <summary>
        /// New sprite instance
        /// </summary>
        /// <param name="Textures">Textures (frames) that this sprite is composed of</param>
        /// <param name="RenderInfo">Rendering Attributes</param>
        /// <param name="Scale">Scale vector (x,y)</param>
        public Sprite( List<Texture2D> Textures, RenderProperties RenderInfo, Vector2 Scale )
        {
            // Assign textures
            _textures = Textures;
            // identify if it is animated or not
            if ( _textures.Count > 1 ) // if > 1, assume animated
                Type = SpriteTypes.Animated;
            // render
            this.RenderInfo = RenderInfo;
            // Scale
            this.Scale = Scale;
            // All other equals default
            // Rotation
            Rotation = 0;
            // Speed
            Speed = 1;
        }
        /// <summary>
        /// New sprite instance
        /// </summary>
        /// <param name="Textures">Textures (frames) that this sprite is composed of</param>
        /// <param name="RenderInfo">Rendering Attributes</param>
        /// <param name="Scale">Scale vector (x,y)</param>
        /// <param name="Rotation">Sprite rotation (rad)</param>
        public Sprite( List<Texture2D> Textures, RenderProperties RenderInfo, Vector2 Scale, float Rotation )
        {
            // Assign textures
            _textures = Textures;
            // identify if it is animated or not
            if ( _textures.Count > 1 ) // if > 1, assume animated
                Type = SpriteTypes.Animated;
            // render
            this.RenderInfo = RenderInfo;
            // Scale
            this.Scale = Scale;
            // Rotation
            this.Rotation = Rotation;
            // All other equals default
            // Speed
            Speed = 1;
        }
        /// <summary>
        /// New sprite instance
        /// </summary>
        /// <param name="Textures">Textures (frames) that this sprite is composed of</param>
        /// <param name="RenderInfo">Rendering Attributes</param>
        /// <param name="Scale">Scale vector (x,y)</param>
        /// <param name="Rotation">Sprite rotation (rad)</param>
        /// <param name="Speed">Animation speed (sec)</param>
        public Sprite( List<Texture2D> Textures, RenderProperties RenderInfo, Vector2 Scale, float Rotation, float Speed )
        {
            // Assign textures
            _textures = Textures;
            // identify if it is animated or not
            if ( _textures.Count > 1 ) // if > 1, assume animated
                Type = SpriteTypes.Animated;
            // render
            this.RenderInfo = RenderInfo;
            // Scale
            this.Scale = Scale;
            // Rotation
            this.Rotation = Rotation;
            // Speed
            this.Speed = Speed;
        }
        #endregion
    }
}