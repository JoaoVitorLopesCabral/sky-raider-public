﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace SkyRaider.Game
{
    /// <summary>
    /// Collision groups
    /// </summary>
    public enum CollisionGroup {
        /// <summary>
        /// Not solid at all
        /// </summary>
        NotSolid,
        /// <summary>
        /// Rock solid!
        /// </summary>
        Solid,
        /// <summary>
        /// For other entities, like fuel depot
        /// </summary>
        Special,
        /// <summary>
        /// AI collision flag
        /// </summary>
        AI,
    };
    /// <summary>
    /// Entity activity states
    /// </summary>
    public enum EntityState { Alive, Dead, Invulnerable };
    /// <summary>
    /// Entity types
    /// </summary>
    public enum EntityType { AI, Logic, Player, Prop };
    /// <summary>
    /// Represents an entity, which can be an object, an enemy unit, the player and other invisible stuff.
    /// </summary>
    public class Entity
    {
        #region Organization
        /// <summary>
        /// GameWorld instance owning this entity
        /// This gives access to gamerule data, assets and other entities
        /// </summary>
        public GameWorld World { get; set; }
        /// <summary>
        /// Is this a Player entity?
        /// </summary>
        public bool IsPlayer { get; set; }
        #endregion

        #region Base Combat Properties
        /// <summary>
        /// Entity current status
        /// </summary>
        public EntityState State { get; set; }
        /// <summary>
        /// Entity health
        /// </summary>
        public int Health { get; set; }
        /// <summary>
        /// Maximum health for this entity
        /// </summary>
        public int MaxHealth { get; set; }
        /// <summary>
        /// Armor value
        /// TODO: Find a way to implement it.
        /// </summary>
        public float ArmorValue { get; set; }
        /// <summary>
        /// Attack speed
        /// </summary>
        public float AttackSpeed { get; set; }
        /// <summary>
        /// Last time this entity attacked something
        /// </summary>
        public double LastAttackTime { get; set; }
        /// <summary>
        /// Rewards the attacker with the given score
        /// </summary>
        /// <param name="Score">Score</param>
        public virtual void GetAttackReward( int Score )
        {
            this.Score += Score;
        }
        /// <summary>
        /// Called when this entity is hit
        /// </summary>
        /// <param name="Attacker"></param>
        public virtual void Hit( Entity Attacker )
        {
            #if DEBUG
            if ( dFlags == DebugFlags.GOD )
                return;
            #endif
            // lower health
            Health--;

            // if it died, award the killer
            if ( Health <= 0 )
            {
                Attacker.GetAttackReward( (int)( ScoreValue * World.Rules.ScoreModifier * ScoreMultiplier ) );
                // set entity state to dead
                State = EntityState.Dead;
            }
        }
        /// <summary>
        /// Hits this entity with the given damage
        /// </summary>
        /// <param name="Attacker"></param>
        /// <param name="Damage"></param>
        public virtual void Hit( Entity Attacker, int Damage )
        {
            #if DEBUG
            if ( dFlags == DebugFlags.GOD )
                return;
            #endif
            // lower health
            Health -= Damage;

            // if it died, award the killer
            if ( Health <= 0 )
            {
                Attacker.Score += (int)( ScoreValue * World.Rules.ScoreModifier * ScoreMultiplier );
                // set entity state to dead
                State = EntityState.Dead;
            }
        }
        #endregion

        #region Debug stuff
        #if DEBUG
        public DebugFlags dFlags;
        #endif
        #endregion

        #region Position and Movement
        /// <summary>
        /// Entity spawn position
        /// </summary>
        public Vector2 SpawnPosition { get; set; }
        /// <summary>
        /// Entity Position
        /// </summary>
        public Vector2 Position;
        /// <summary>
        /// Entity center point
        /// </summary>
        public Vector2 Origin
        {
            get
            {
                // if sprite is not null (if it is, use entity position)
                if ( Sprite == null )
                    return Position;

                // return the center point of the current frame
                Texture2D frameRef = Sprite.getFrame();

                // return the correct point
                return new Vector2()
                {
                    X = Position.X + frameRef.Width / 2,
                    Y = Position.Y + frameRef.Height / 2,
                };
            }
        }
        /// <summary>
        /// Current movement direction
        /// </summary>
        public Vector2 Direction { get; set; }
        #endregion

        #region Appearance
        /// <summary>
        /// Animation time (elapsed time)
        /// </summary>
        public double AnimTime { get; set; }
        /// <summary>
        /// Death animation time
        /// </summary>
        public double DeathAnimTime { get; set; }
        /// <summary>
        /// Entity visual representation
        /// </summary>
        public Sprite Sprite { get; set; }
        /// <summary>
        /// Death Animation
        /// </summary>
        public Sprite DeathAnim { get; set; }
        /// <summary>
        /// Draws the entity
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void Draw( SpriteBatch spriteBatch )
        {
            if ( State == EntityState.Dead )
            {
                // check if it has death animation
                if ( DeathAnim.getFrameCount() <= 0 )
                    return;
                if ( deathAnimFrames >= DeathAnim.getFrameCount() )
                    return;
                spriteBatch.Draw( DeathAnim.getFrame(), Position, null, DeathAnim.RenderInfo.TintingColor,
                    DeathAnim.Rotation, Vector2.Zero, DeathAnim.Scale, SpriteEffects.None, 0 );
            }
            else
            {
                spriteBatch.Draw( Sprite.getFrame(), Position, null, Sprite.RenderInfo.TintingColor,
                        Sprite.Rotation, Vector2.Zero, Sprite.Scale, SpriteEffects.None, 0 );
            }
        }
        #endregion

        #region Scores
        /// <summary>
        /// Total points given if killed
        /// </summary>
        public int ScoreValue { get; set; }
        /// <summary>
        /// Score point multiplier (if buffed entity)
        /// </summary>
        public float ScoreMultiplier { get; set; }
        /// <summary>
        /// Score! 
        /// </summary>
        public int Score { get; set; }
        #endregion

        #region Behavior
        /// <summary>
        /// Entity ability to think
        /// </summary>
        public bool ShouldThink { get; set; }
        /// <summary>
        /// Think time
        /// </summary>
        public float ThinkTime { get; set; }
        /// <summary>
        /// Last time Think() was called.
        /// </summary>
        public double LastThinkTime { get; set; }
        /// <summary>
        /// Think!
        /// </summary>
        public virtual void Think() {  }
        /// <summary>
        /// Last update time
        /// </summary>
        protected double LastUpdate { get; set; }
        /// <summary>
        /// Total running time (reference for animation)
        /// </summary>
        protected double animTotalTime { get; set; }
        /// <summary>
        /// Total death animation frames ran
        /// </summary>
        protected int deathAnimFrames { get; set; }
        /// <summary>
        /// Everything a entity can do is defined here.
        /// </summary>
        /// <param name="gameTime">gametime stuff</param>
        public virtual void Update( GameTime gameTime )
        {
            // check if the game is running, otherwise don't do anything
            if ( World.Rules.State == GameState.Waiting )
                return;

            if ( State == EntityState.Dead )
            {
                if ( deathAnimFrames > DeathAnim.getFrameCount() )
                {
                    // entity dead and has finished running death animation, remove it
                    Kill();
                }

                DeathAnimTime += gameTime.ElapsedGameTime.TotalMilliseconds;

                if ( DeathAnimTime >= DeathAnim.Speed )
                {
                    DeathAnim.SetNextFrame();
                    DeathAnimTime = 0;
                    // update frame count
                    deathAnimFrames++;
                }

            }

            AnimTime += gameTime.ElapsedGameTime.TotalMilliseconds;

            if ( AnimTime >= Sprite.Speed )
            {
                Sprite.SetNextFrame();
                AnimTime = 0;
            }

            // think
            if ( gameTime.TotalGameTime.TotalSeconds - LastThinkTime > ThinkTime )
            {
                // think
                Think();
                // store time
                LastThinkTime = gameTime.TotalGameTime.TotalSeconds;
            }

            // vertical movement
            // get a normalized copy of the movement direction vector
            Vector2 normMov = Direction;
            // normalize it
            normMov.Normalize();
            // move
            Position += World.Rules.Speed * World.Rules.SpeedBoost * normMov;

            // kill if it is beyond sight
            if ( Position.Y > 1000 )
                Kill();
        }
        /// <summary>
        /// Removes the entity from the game
        /// </summary>
        public virtual void Kill()
        {
            World.Entities.Remove( this );
        }
        #endregion



        #region Flags
        /// <summary>
        /// Entity collision group
        /// </summary>
        public CollisionGroup Collision { get; set; }

        #endregion

        #region Other Function
        /// <summary>
        /// Entity collision bounds
        /// </summary>
        /// <returns></returns>
        public virtual Rectangle GetBounds()
        {
            // retrieve rectangle based on the dimenions * scale
            return new Rectangle( (int)Position.X, (int)Position.Y,
                (int)( Sprite.getFrame().Width * Sprite.Scale.X ), (int)( Sprite.getFrame().Height * Sprite.Scale.Y ) );
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Empty constructor for class inheritance
        /// </summary>
        public Entity()
        {
            // default move 
            Direction = new Vector2( 0, 1 );
            // also not a player :)
            IsPlayer = false;
            // init sprite 
            Sprite = new Sprite( new List<Texture2D>() );
            // death anim
            DeathAnim = new Sprite( new List<Texture2D>() );
            // Score multiplier
            ScoreMultiplier = 1;
        }
        /// <summary>
        /// Creates a new entity!
        /// </summary>
        public Entity( GameWorld World )  
        { 
            // default move 
            Direction = new Vector2( 0, 1 );
            // also not a player :)
            IsPlayer = false;
            // init sprite 
            Sprite = new Sprite( new List<Texture2D>() );
            // death anim
            DeathAnim = new Sprite( World.AnimationAssets[ "explosion" ] );
            // default animation speed
            DeathAnim.Speed = 200f;
            // Score multiplier
            ScoreMultiplier = 1;
        }
        #endregion
    }

    /// <summary>
    /// Base class for power ups (including fuel)
    /// </summary>
    public class PowerUpEntity : Entity
    {
    }
    /// <summary>
    /// Base class for enviromental entities (supports drawing and animations)
    /// </summary>
    public class EnviromentalEntity : Entity
    {
    }
    /// <summary>
    /// Base class for logical entities
    /// </summary>
    public class LogicalEntity : Entity
    {
        #region Functions
        /// <summary>
        /// Overrides base drawing (this entity is invisible)
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw( SpriteBatch spriteBatch )
        {
            
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Setup default flags for logical entities
        /// </summary>
        public LogicalEntity()
        {
            Collision = CollisionGroup.Special;
        }
        #endregion
    }
}