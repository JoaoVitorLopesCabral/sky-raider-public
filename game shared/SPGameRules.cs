﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Gamerules for Single-Player mode
    /// </summary>
    public class SPGameRules : GameRules
    {
        #region Constructor
        public SPGameRules( GameDifficulty Difficulty )
            : base( Difficulty )
        {
            // Set max players to 1
            MaxPlayers = 1;
        }
        #endregion
    }
}