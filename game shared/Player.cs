﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Defines a player!
    /// </summary>
    public class Player : CombatEntity
    {
        #region Properties
        /// <summary>
        /// Player Name
        /// </summary>
        public string PlayerName { get; set; }
        /// <summary>
        /// Available credits or lives
        /// </summary>
        public int Lives { get; set; }
        /// <summary>
        /// Plane fuel
        /// </summary>
        public float Fuel { get; set; }
        /// <summary>
        /// Fuel update time
        /// </summary>
        public double FuelUpdate { get; set; }
        #endregion

        #region Engine
        /// <summary>
        /// Engine steam
        /// </summary>
        Sprite EngineSteam { get; set; }
        /// <summary>
        /// Engine animation
        /// </summary>
        double EngineAnimeTime { get; set; }
        #endregion

        #region Movement stuff
        /// <summary>
        /// movement bounds
        /// </summary>
        public Rectangle playerBounds;
        /// <summary>
        /// bounds textures
        /// </summary>
        public Sprite boundText;
        /// <summary>
        /// last bounds save time
        /// </summary>
        public double lastSave;
        /// <summary>
        /// Draw movement bounds
        /// </summary>
        public bool DrawBounds;
        /// <summary>
        /// last toggle time
        /// </summary>
        public double boundsToggle;
        /// <summary>
        /// Forward speed
        /// </summary>
        public float ForwardSpeed { get; set; }
        /// <summary>
        /// Backward speed
        /// </summary>
        public float BackwardSpeed { get; set; }
        /// <summary>
        /// Sideways speed
        /// </summary>
        public float SideSpeed { get; set; }
        #endregion

        #region Spawn invulnerability
        /// <summary>
        /// Spawning time
        /// </summary>
        public double spawnTime;
        /// <summary>
        /// Toggle blinking values (0 full opaque - 1 transparent)
        /// </summary>
        public byte blinkStatus;
        /// <summary>
        /// Blinking time
        /// </summary>
        public double lastBlink;
        /// <summary>
        /// total game seconds
        /// </summary>
        private GameTime gameSeconds;
        #endregion

        #region Score System
        /// <summary>
        /// Player level
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// Total distance travelled
        /// </summary>
        public int DistanceTravelled { get; set; }
        /// <summary>
        /// Attack scores
        /// </summary>
        public int KillScore { get { return Score; } }
        #endregion


        #region Functions
        /// <summary>
        /// Gets a value representing the current level progress
        /// </summary>
        /// <returns></returns>
        public float GetLevelProgress()
        {
            float nextxp = World.Rules.GetTotalExperience( Level + 1 );
            float prevxp = ( Level == 1 ) ? 0 : World.Rules.GetTotalExperience( Level );
            float pg = ( Score - prevxp ) / ( nextxp - prevxp );
            return pg;
        }
        /// <summary>
        /// Get points from killing an entity
        /// </summary>
        /// <param name="Score"></param>
        public override void GetAttackReward( int Score )
        {
            base.GetAttackReward( Score );

            // check if we have levelled up
            if ( this.Score >= World.Rules.GetTotalExperience( Level + 1 ) )
                Level++;
        }
        /// <summary>
        /// Respawns the player if there is any live left
        /// </summary>
        public void Respawn()
        {
            // Respawns the player in the center of its movement bounds
            Position = new Vector2( playerBounds.Left + ( playerBounds.Width / 2 ), playerBounds.Top + ( playerBounds.Height / 2 ) );
            // Set invulnerable
            State = EntityState.Invulnerable;
            // spawn time
            this.spawnTime = gameSeconds.TotalGameTime.TotalSeconds;
        }
        /// <summary>
        /// Player hit logic
        /// </summary>
        /// <param name="Attacker"></param>
        public override void Hit( Entity Attacker )
        {
            if ( State == EntityState.Invulnerable )
                return;

            base.Hit( Attacker );

            if ( Health <= 0 && Lives > 0 )
            {
                Lives--;
                Health = 1;
                Fuel = 1;
                // set state to player death
                World.Rules.SetState( GameState.PlayerDeath );
                // Since many stuff has changed, we will just respawn the player
                Respawn();
            }
        }
        /// <summary>
        /// Player hit logic
        /// </summary>
        /// <param name="Attacker"></param>
        public override void Hit( Entity Attacker, int Damage )
        {
            if ( State == EntityState.Invulnerable )
                return;

            base.Hit( Attacker, Damage );

            if ( Health <= 0 && Lives > 0 )
            {
                Lives--;
                Health = 1;
                Fuel = 1;
                // set state to player death
                World.Rules.SetState( GameState.PlayerDeath );
                // Since many stuff has changed, we will just respawn the player
                Respawn();
            }
        }
        /// <summary>
        /// Handles player inputs - Not like other entities, player has no Think Function
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update( GameTime gameTime )
        {
            // update game seconds;
            gameSeconds = gameTime;

            if ( World.Rules.State == GameState.GameRunning )
            {
                // dead?
                if ( Health <= 0 && Lives < 1 )
                {
                    //World.Players.Remove( this );
                    // Instead of removing entity, just set state to game over
                    World.Rules.SetState( GameState.GameOver );
                    // stop execution
                    return;
                }

                // collision
                // if invulnerable, ignore collision
                if ( State == EntityState.Alive )
                {
                    // entites
                    foreach ( Entity ent in World.Entities )
                    {
                        // if its the remains of an entity (explosion, etc) dont collide
                        if ( ent.State == EntityState.Dead )
                            continue;
                        // check collision group
                        if ( ent.Collision == CollisionGroup.NotSolid )
                            continue;
                        if ( ent.Collision == CollisionGroup.Special )
                            continue;
                        // get bounds
                        Rectangle bounds = ent.GetBounds();

                        if ( bounds.Intersects( this.GetBounds() ) )
                        {
                            // kill each other
                            ent.Hit( this ); 
                            Hit( ent );
                        }
                    }
                    // AI
                    foreach ( Entity ent in World.AIUnits )
                    {
                        // if its the remains of an entity (explosion, etc) dont collide
                        if ( ent.State == EntityState.Dead )
                            continue;
                        // check collision group
                        if ( ent.Collision == CollisionGroup.NotSolid )
                            continue;
                        if ( ent.Collision == CollisionGroup.Special )
                            continue;
                        // get bounds
                        Rectangle bounds = ent.GetBounds();

                        if ( bounds.Intersects( this.GetBounds() ) )
                        {
                            // kill each other
                            ent.Hit( this );
                            Hit( ent );
                        }
                    }
                }
                
                // invulnerability blinking
                if ( State == EntityState.Invulnerable )
                {
                    // toggle blinking value
                    if ( gameTime.TotalGameTime.TotalSeconds - lastBlink > 0.25 )
                    {
                        if ( blinkStatus == 0 )
                            blinkStatus = 1;
                        else
                            blinkStatus = 0;

                        lastBlink = gameTime.TotalGameTime.TotalSeconds;
                    }
                    // see if it should lose the invulnerable status
                    if ( gameTime.TotalGameTime.TotalSeconds - spawnTime >= 5 )
                    {
                        State = EntityState.Alive;
                    }
                }
            }
            // fuel
            if ( Fuel <= 0 )
                Hit( this );
            if ( Fuel > 0 && gameTime.TotalGameTime.TotalSeconds - FuelUpdate > 1.0f && World.Rules.State == GameState.GameRunning )
            {
                #if WINDOWS
                if ( Keyboard.GetState().IsKeyDown( Keys.Up ) )
                    Fuel -= 0.08f;
                else if ( Keyboard.GetState().IsKeyDown( Keys.Down ) )
                    Fuel -= 0.03f;
                else
                    Fuel -= 0.05f;
                #elif XBOX
                if ( GamePad.GetState( PlayerIndex.One ).IsButtonDown(Buttons.DPadUp) || GamePad.GetState( PlayerIndex.One ).ThumbSticks.Left.Y >= 1 )
                    Fuel -= 0.08f;
                else if ( GamePad.GetState( PlayerIndex.One ).IsButtonDown( Buttons.DPadUp ) || GamePad.GetState( PlayerIndex.One ).ThumbSticks.Left.Y <= -1 )
                    Fuel -= 0.03f;
                else
                    Fuel -= 0.05f;
                #endif
                FuelUpdate = gameTime.TotalGameTime.TotalSeconds;
            }

            AnimTime += gameTime.ElapsedGameTime.TotalMilliseconds;

            if ( AnimTime >= Sprite.Speed )
            {
                Sprite.SetNextFrame();
                AnimTime = 0;
            }

            #if WINDOWS
            // Gamestates are defined within the game rules class, so if the state says pause or something that does not allow movement, the update function won't be called.
            // read keyboard state
            KeyboardState kpad = Keyboard.GetState();

            // Movement -- 
            // Player can move up/down left/right within a pre defined area, but the rest of the world will keep moving like a vertical scroller
            if ( kpad.IsKeyDown( Keys.Up ) )
            {
                // increment steam speed
                EngineSteam.Speed = 175f;
                // Moves up to 256
                if ( Position.Y > playerBounds.Top )
                    Position.Y -= ForwardSpeed;
                // set game to running
                World.Rules.SetState( GameState.GameRunning );
            }
            else if ( kpad.IsKeyDown( Keys.Down ) )
            {
                // reduce steam speed
                EngineSteam.Speed = 400f;
                if ( Position.Y < playerBounds.Bottom )
                    Position.Y += BackwardSpeed;
                // set game to running
                World.Rules.SetState( GameState.GameRunning );
            }
            // Side moving
            if ( kpad.IsKeyDown( Keys.Left ) )
            {
                if ( Position.X > playerBounds.Left )
                    Position.X -= SideSpeed;
                // sprite
                Sprite.SetFrame( World.TextureAssets[ "player_left" ] );
                // set game to running
                World.Rules.SetState( GameState.GameRunning );
            }
            else if ( kpad.IsKeyDown( Keys.Right ) )
            {
                if ( Position.X < playerBounds.Right )
                    Position.X += SideSpeed;
                // sprite
                Sprite.SetFrame( World.TextureAssets[ "player_right" ] );
                // set game to running
                World.Rules.SetState( GameState.GameRunning );
            }
            else
            {
                // idle texture
                Sprite.SetFrame( World.TextureAssets[ "player_idle" ] );
                // speed
                EngineSteam.Speed = 250.0f;
                World.Rules.SpeedBoost = 1.0f;
            }

            // some commands
            if ( kpad.IsKeyDown( Keys.P ) )
            {
                World.Rules.SetState( GameState.Paused );
            }
            if ( kpad.IsKeyDown( Keys.U ) )
            {
                Score += 100;
            }

            #if DEBUG
            if ( kpad.IsKeyDown( Keys.F5 ) || kpad.IsKeyDown( Keys.Add ) )
            {
                // add 1 to the bounds X
                playerBounds.Width += 1;
            }
            if ( kpad.IsKeyDown( Keys.F6 ) || kpad.IsKeyDown( Keys.Subtract ) )
            {
                // remove 1 from the x
                playerBounds.Width -= 1;
            }
            if ( kpad.IsKeyDown( Keys.F7 ) || kpad.IsKeyDown( Keys.Multiply ) )
            {
                // add 1 to the bounds Y
                playerBounds.Height += 1;
            }
            if ( kpad.IsKeyDown( Keys.F8 ) || kpad.IsKeyDown( Keys.Divide ) )
            {
                // subtracts 1 from y
                playerBounds.Height -= 1;
            }
            if ( kpad.IsKeyDown( Keys.NumPad8 ) )
            {
                // move bounds up
                playerBounds.Y -= 1;
            }
            if ( kpad.IsKeyDown( Keys.NumPad2 ) )
            {
                // move bounds down
                playerBounds.Y += 1;
            }
            if ( kpad.IsKeyDown( Keys.NumPad4 ) )
            {
                // move bounds left
                playerBounds.X -= 1;
            }
            if ( kpad.IsKeyDown( Keys.NumPad6 ) )
            {
                // move bounds right
                playerBounds.X += 1;
            }
            if ( kpad.IsKeyDown( Keys.F9 ) && ( gameTime.TotalGameTime.TotalSeconds - lastSave > 1.0 ) )
            {
                using ( System.IO.StreamWriter sw = new System.IO.StreamWriter( Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments ) + @"\sky_raider_player_bounds.txt" ) )
                {
                    sw.Write( playerBounds.ToString() );
                    sw.Close();
                }
                lastSave = gameTime.TotalGameTime.TotalSeconds;
            }
            if ( kpad.IsKeyDown( Keys.F10 ) && ( gameTime.TotalGameTime.TotalSeconds - boundsToggle > 0.5f ) )
            {
                DrawBounds = ( DrawBounds ) ? false : true;
                boundsToggle = gameTime.TotalGameTime.TotalSeconds;
            }
            #endif
            // Firing
            if ( kpad.IsKeyDown( Keys.Space ) && ( gameTime.TotalGameTime.TotalSeconds - LastAttack > AttackSpeed ) )
            {
                Attack();
                LastAttack = gameTime.TotalGameTime.TotalSeconds;
                // sets the game to active
                World.Rules.SetState( GameState.GameRunning );
            }

            #endif

            #if XBOX
            // XBOX
            // gamepad state - Controller 1 for SP - Multiplayer version will be handled in another class
            GamePadState gpad = GamePad.GetState( PlayerIndex.One );

            // movement
            if ( gpad.IsButtonDown( Buttons.DPadUp ) || gpad.ThumbSticks.Left.Y >= 1 )
            {
                // increment steam speed
                EngineSteam.Speed = 175f;
                // Moves up to 256
                if ( Position.Y > playerBounds.Top )
                    Position.Y -= ForwardSpeed;
                // set game to running
                World.Rules.SetState( GameState.GameRunning );
            }
            else if ( gpad.IsButtonDown( Buttons.DPadDown ) || gpad.ThumbSticks.Left.Y <= -1 )
            {
                // reduce steam speed
                EngineSteam.Speed = 400f;
                if ( Position.Y < playerBounds.Bottom )
                    Position.Y += BackwardSpeed;
                // set game to running
                World.Rules.SetState( GameState.GameRunning );
            }
            // side
            if ( gpad.IsButtonDown( Buttons.DPadLeft ) || gpad.ThumbSticks.Left.X <= -1 )
            {
                if ( Position.X > playerBounds.Left )
                    Position.X -= SideSpeed;
                // sprite
                Sprite.SetFrame( World.TextureAssets[ "player_left" ] );
                // set game to running
                World.Rules.SetState( GameState.GameRunning );
            }
            else if ( gpad.IsButtonDown( Buttons.DPadRight ) || gpad.ThumbSticks.Left.X >= 1 )
            {
                if ( Position.X < playerBounds.Right )
                    Position.X += SideSpeed;
                // sprite
                Sprite.SetFrame( World.TextureAssets[ "player_right" ] );
                // set game to running
                World.Rules.SetState( GameState.GameRunning );
            }
            else
            {
                // idle texture
                Sprite.SetFrame( World.TextureAssets[ "player_idle" ] );
                // speed
                EngineSteam.Speed = 1.0f;
                World.Rules.SpeedBoost = 1.0f;
            }

            // firing
            if ( gpad.IsButtonDown( Buttons.X ) && ( gameTime.TotalGameTime.Milliseconds - LastUpdate > AttackSpeed ) )
                Attack();
#endif

            // Engine animation - Ignore it until we have a better animation
            /*EngineAnimeTime += gameTime.ElapsedGameTime.TotalMilliseconds;

            if ( EngineAnimeTime >= EngineSteam.Speed )
            {
                EngineSteam.SetNextFrame();
                EngineAnimeTime = 0;
            }*/

            // update bullet data
            for ( int i = 0; i < Bullets.Count; i++ )
            {
                Bullets[ i ].Update( gameTime );
            }

            // last update
            LastUpdate = gameTime.ElapsedGameTime.TotalSeconds;

        }
        /// <summary>
        /// Draws the player!
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw( SpriteBatch spriteBatch )
        {
            // bounds
            if ( DrawBounds )
                spriteBatch.Draw( boundText.getFrame(), new Rectangle(playerBounds.Left, playerBounds.Top, playerBounds.Width + 28, playerBounds.Height + 26), boundText.RenderInfo.TintingColor * boundText.RenderInfo.Alpha );
            // bullets
            for ( int i = 0; i < Bullets.Count; i++ )
            {
                Bullets[ i ].Draw( spriteBatch );
            }

            // draw entity based on its state
            if ( State == EntityState.Alive )
            {
                // player
                spriteBatch.Draw( Sprite.getFrame(), Position, Sprite.RenderInfo.TintingColor );
                // draw steam
                /*if ( World.Rules.State == GameState.GameRunning )
                    spriteBatch.Draw( EngineSteam.getFrame(), Position + new Vector2( 0, Sprite.getFrame().Height ), Color.White );*/
            }
            else if ( State == EntityState.Invulnerable )
            {
                switch ( blinkStatus )
                {
                    case 0:
                        spriteBatch.Draw( Sprite.getFrame(), Position, Sprite.RenderInfo.TintingColor * 0.50f );
                        break;
                    case 1:
                        spriteBatch.Draw( Sprite.getFrame(), Position, Sprite.RenderInfo.TintingColor );
                        break;
                }
            }

        }
        /// <summary>
        /// Handle Attack actions
        /// </summary>
        public override void Attack()
        {
            // Create a bullet within player origin
            BaseBullet bullet = new BaseBullet( this );
            // set target
            bullet.Target = Origin + ( new Vector2( Origin.X, Origin.Y + 200.0f ) ); // should change, value just for testing

            Bullets.Add( bullet );
        }
        #endregion

        #region Constructors
        /// <summary>
        /// New Player Instance
        /// </summary>
        /// <param name="World">Game World</param>
        public Player( GameWorld World )
        {
            this.World = World;
            Bullets = new List<Entity>();
            MovementSpeed = 4.0f;
            Sprite = new Sprite( new List<Texture2D>() { World.TextureAssets[ "player_idle" ], World.TextureAssets[ "player_left" ],
                World.TextureAssets[ "player_right" ] } );
            DeathAnim = new Sprite( World.AnimationAssets[ "explosion" ] );
            AttackSpeed = 0.5f;
            //Sequence.Speed = 0.25f;
            Lives = 3;
            Fuel = 1.0f;
            // engine steam
            EngineSteam = new Sprite( World.AnimationAssets[ "plane_engine" ] );
            EngineSteam.Speed = 250f;
            // bounds
            //playerBounds = new Rectangle( 140, 57, 505, 450);
            //playerBounds = new Rectangle( 12, 7, 1236 - 63, 760 - 48 );
            playerBounds = new Rectangle( 12, 7, World.ClientInfo.ScreenWidth - Sprite.getFrame().Width,
                World.ClientInfo.ScreenHeight - Sprite.getFrame().Height );
            boundText = new Sprite( new List<Texture2D>() { World.TextureAssets[ "player_bounds" ] } );
            boundText.RenderInfo.Alpha = 0.70f;
            // Speed
            ForwardSpeed = 2.5f;
            BackwardSpeed = 2.0f;
            SideSpeed = 5.0f;
            // And Yes, this is a player entity
            IsPlayer = true;
            // starts at level 1
            Level = 1;
        }
        #endregion
    }
}