﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/*
 *  Sprite classes
 *  To support both static and animated entities
 * */

namespace SkyRaider.Game
{
    /// <summary>
    /// Special rendering attributes
    /// </summary>
    public class RenderProperties
    {
        #region Properties
        /// <summary>
        /// Color to use the tinting effect, use White for original colors
        /// </summary>
        public Color TintingColor { get; set; }
        /// <summary>
        /// Alpha levels
        /// </summary>
        public float Alpha { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// New instance of RenderProperties Class. With default values (Full tinting and Opaque)
        /// </summary>
        public RenderProperties() { TintingColor = Color.White; Alpha = 1.0f; }
        /// <summary>
        /// New instance of RenderProperties Class.
        /// </summary>
        /// <param name="Tinting">Tinting color</param>
        public RenderProperties( Color Tinting ) { TintingColor = Tinting; Alpha = 1.0f; }
        /// <summary>
        /// New instance of RenderProperties Class.
        /// </summary>
        /// <param name="Tinting">Tinting color</param>
        /// <param name="Alpha">Alpha value</param>
        public RenderProperties( Color Tinting, float Alpha ) { TintingColor = Tinting; this.Alpha = Alpha; }
        #endregion
    }
    /// <summary>
    /// Represents a static image within an object
    /// </summary>
    public class SpriteOld
    {
        #region Properties
        /// <summary>
        /// Texture
        /// </summary>
        public Texture2D Texture { get; set; }
        /// <summary>
        /// Texture scale
        /// </summary>
        public float Scale { get; set; }
        /// <summary>
        /// Texture rotation
        /// </summary>
        public float Rotation;
        /// <summary>
        /// Rendering properties
        /// </summary>
        public RenderProperties Render { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Start a new instance of Sprite class.
        /// <param name="Texture">Sprite Texture</param>
        /// </summary>
        public SpriteOld( Texture2D Texture ) { this.Texture = Texture; Scale = 1; Rotation = 0; Render = new RenderProperties(); }
        /// <summary>
        /// Start a new instance of Sprite class.
        /// </summary>
        /// <param name="Texture">Sprite Texture</param>
        /// <param name="Scale">Texture scale</param>
        public SpriteOld( Texture2D Texture, float Scale ) { this.Texture = Texture; this.Scale = Scale; Rotation = 0; Render = new RenderProperties(); }
        /// <summary>
        /// Start a new instance of Sprite class.
        /// </summary>
        /// <param name="Texture">Sprite Texture</param>
        /// <param name="Scale">Texture scale</param>
        /// <param name="Rotation">Texture Rotation</param>
        public SpriteOld( Texture2D Texture, float Scale, float Rotation ) { this.Texture = Texture; this.Scale = Scale; this.Rotation = Rotation; Render = new RenderProperties(); }
        /// <summary>
        /// Start a new instance of Sprite class.
        /// </summary>
        /// <param name="Texture">Sprite Texture</param>
        /// <param name="Scale">Texture scale</param>
        /// <param name="Rotation">Texture Rotation</param>
        /// <param name="Render">Render values</param>
        public SpriteOld( Texture2D Texture, float Scale, float Rotation, RenderProperties Render ) { this.Texture = Texture; this.Scale = Scale; this.Rotation = Rotation; this.Render = Render; }
        #endregion
    }
    /// <summary>
    /// Represents a sprite with a collection of images (animated)
    /// </summary>
    public class AnimatedSpriteOld
    {
        #region Properties
        /// <summary>
        /// Array of images representing the sprite
        /// </summary>
        public List<Texture2D> Textures { get; set; }
        /// <summary>
        /// Animation Speed
        /// </summary>
        public float Speed { get; set; }
        /// <summary>
        /// Sprite rotation
        /// </summary>
        public float Rotation { get; set; }
        /// <summary>
        /// Sprite Scale
        /// </summary>
        public float Scale { get; set; }
        /// <summary>
        /// Rendering attributes
        /// </summary>
        public RenderProperties Render { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Start a new instance of AnimatedSprite class.
        /// <param name="Texture">Sprite frames</param>
        /// </summary>
        public AnimatedSpriteOld( List<Texture2D> Textures ) { this.Textures = Textures; Scale = 1; Rotation = 0; Render = new RenderProperties(); }
        /// <summary>
        /// Start a new instance of AnimatedSprite class.
        /// </summary>
        /// <param name="Texture">Sprite frames</param>
        /// <param name="Scale">Texture scale</param>
        public AnimatedSpriteOld( List<Texture2D> Textures, float Scale ) { this.Textures = Textures; this.Scale = Scale; Rotation = 0; Render = new RenderProperties(); }
        /// <summary>
        /// Start a new instance of AnimatedSprite class.
        /// </summary>
        /// <param name="Texture">Sprite frames</param>
        /// <param name="Scale">Texture scale</param>
        /// <param name="Rotation">Texture Rotation</param>
        public AnimatedSpriteOld( List<Texture2D> Textures, float Scale, float Rotation ) { this.Textures = Textures; this.Scale = Scale; this.Rotation = Rotation; Render = new RenderProperties(); }
        /// <summary>
        /// Start a new instance of AnimatedSprite class.
        /// </summary>
        /// <param name="Texture">Sprite frames</param>
        /// <param name="Scale">Texture scale</param>
        /// <param name="Rotation">Texture Rotation</param>
        /// <param name="Render">Render values</param>
        public AnimatedSpriteOld( List<Texture2D> Textures, float Scale, float Rotation, RenderProperties Render ) { this.Textures = Textures; this.Scale = Scale; Rotation = 0; this.Render = Render; }
        #endregion
    }
}