using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Holds game client info
    /// </summary>
    public class Client
    {
        #region Screen Properties
        /// <summary>
        /// Client Screen Width
        /// </summary>
        public int ScreenWidth { get; set; }
        /// <summary>
        /// Client Screen Height
        /// </summary>
        public int ScreenHeight { get; set; }
        /// <summary>
        /// Top left
        /// </summary>
        public Vector2 TopLeft { get { return Vector2.Zero; } }
        /// <summary>
        /// Bottom Right corner
        /// </summary>
        public Vector2 BottomRight { get { return new Vector2( ScreenWidth, ScreenHeight ); } }
        #endregion

        #region Functions
        /// <summary>
        /// Get a vector representing the coordinates at the given screen %
        /// Values above 100% will result in a offscreen position
        /// </summary>
        /// <param name="percentagex">Width percentage</param>
        /// <param name="percentagey">Height percentage</param>
        /// <returns></returns>
        public Vector2 GetRelativePosition( float percentagex, float percentagey )
        {
            float x = ( percentagex / 100 ) * ScreenWidth;
            float y = ( percentagey / 100 ) * ScreenHeight;

            return new Vector2( x, y );
        }
        /// <summary>
        /// Get a centralized coordinate for the given element length
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public float CenterElement( float length )
        {
            // get the dif
            float dx = ScreenWidth - length;

            return dx / 2;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create a new client info instance
        /// </summary>
        /// <param name="Width">Screen width</param>
        /// <param name="Height">Screen height</param>
        public Client( int Width, int Height )
        {
            ScreenWidth = Width;
            ScreenHeight = Height;
        }
        #endregion
    }
}