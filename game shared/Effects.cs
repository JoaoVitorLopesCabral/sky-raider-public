﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Entity that gives Area damage when triggered
    /// </summary>
    public class Explosion : LogicalEntity
    {
        #region Properties
        /// <summary>
        /// Explosion Radius
        /// </summary>
        protected int Radius { get; set; }
        /// <summary>
        /// Explosion Damage
        /// </summary>
        protected int Damage { get; set; }
        #endregion

        #region Functions
        /// <summary>
        /// Check if any part of the given entity is within blast radius
        /// </summary>
        /// <param name="ent">Target entity</param>
        /// <returns></returns>
        private bool IsInRange( Entity ent )
        {
            // get entity bounds
            Rectangle rect = ent.GetBounds();

            // check if any part of the rect intersects with the blast
            if ( Vector2.Distance( new Vector2( rect.Left, rect.Top ), Position ) <= Radius )
                return true;
            else if ( Vector2.Distance( new Vector2( rect.Left, rect.Bottom ), Position ) <= Radius )
                return true;
            else if ( Vector2.Distance( new Vector2( rect.Right, rect.Top ), Position ) <= Radius )
                return true;
            else if ( Vector2.Distance( new Vector2( rect.Right, rect.Bottom ), Position ) <= Radius )
                return true;
            else
                return false;
        }
        /// <summary>
        /// Triggers the explosion
        /// </summary>
        public void Trigger()
        {
            // look for all entities within range
            /*foreach ( Entity ent in World.Players )
            {
                if ( Vector2.Distance( ent.Position, Position ) <= Radius )
                    ( (Player)ent ).Hit( this, Damage );
            }
            foreach ( Entity ent in World.AIUnits )
            {
                if ( Vector2.Distance( ent.Position, Position ) <= Radius )
                    ent.Hit( this, Damage );
            }
            foreach ( Entity ent in World.Entities )
            {
                if ( Vector2.Distance( ent.Position, Position ) <= Radius )
                    ent.Hit( this, Damage );
            }*/
            foreach ( Entity ent in World.Players )
            {
                if ( IsInRange( ent ) )
                    ent.Hit( this, Damage );
            }
            foreach ( Entity ent in World.AIUnits )
            {
                if ( IsInRange( ent ) )
                    ent.Hit( this, Damage );
            }
            foreach ( Entity ent in World.Entities )
            {
                if ( IsInRange( ent ) )
                    ent.Hit( this, Damage );
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new Explosion entity
        /// </summary>
        /// <param name="Position">Position</param>
        /// <param name="Damage">Total Damage</param>
        /// <param name="Radius">Effect radius</param>
        /// <param name="World">World</param>
        public Explosion( Vector2 Position, int Damage, int Radius, GameWorld World )
        {
            this.Position = Position;
            this.Damage = Damage;
            this.Radius = Radius;
            this.World = World;
        }
        #endregion
    }
}