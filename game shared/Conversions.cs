﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Constains useful functions
    /// </summary>
    public static partial class Util
    {
        #region Point Related
        /// <summary>
        /// Converts Vector2 to Point
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static Point ToPoint( Vector2 vector )
        {
            return new Point()
            {
                X = (int)vector.X,
                Y = (int)vector.Y,
            };
        }
        /// <summary>
        /// Converts a Point to Vector2
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector2 ToVector2( Point point )
        {
            return new Vector2()
            {
                X = point.X,
                Y = point.Y,
            };
        }
        #endregion

        #region Collections
        /// <summary>
        /// Converts the given array to a list(T) of the same type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<T> ToList<T>( T[] data )
        {
            // create temp list
            List<T> temp = new List<T>( data.Length );
            // loop through array and fill list
            for ( int i = 0; i < data.Length; i++ )
            {
                temp.Add( data[ i ] );
            }

            return temp;
        }
        #endregion
    }
}