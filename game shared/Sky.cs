﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Represents the sky
    /// </summary>
    public class Sky : Entity
    {
        #region Properties
        /// <summary>
        /// Bottom-most layer, representing the terrain.
        /// </summary>
        public Sprite Terrain { get; set; }
        /// <summary>
        /// Terrain pos
        /// </summary>
        public Vector2 TerrainPosition;
        /// <summary>
        /// Terrain scrolling speed
        /// </summary>
        public float TerrainSpeed { get; set; }
        #endregion

        #region Functions
        /// <summary>
        /// Update - Moving logic
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update( GameTime gameTime )
        {
            // moves bottom layer
            TerrainPosition.Y += TerrainSpeed;
            // -- test
            TerrainPosition.Y = TerrainPosition.Y % Terrain.getFrame().Height;
        }
        /// <summary>
        /// Drawing
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw( SpriteBatch spriteBatch )
        {
            // -- if sky is not over screen
            if ( TerrainPosition.Y < World.ClientInfo.ScreenHeight )
                spriteBatch.Draw( Terrain.getFrame(), TerrainPosition, null, Terrain.RenderInfo.TintingColor * Terrain.RenderInfo.Alpha, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0 );
            
            // -- draw the texture again to create a loop
            spriteBatch.Draw( Terrain.getFrame(), TerrainPosition - new Vector2( 0, Terrain.getFrame().Height ), null, Terrain.RenderInfo.TintingColor * Terrain.RenderInfo.Alpha,
                0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0 );
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Sample class constructor (testing only)
        /// </summary>
        public Sky(GameWorld World)
        {
            // world
            this.World = World;
            // terrain
            Terrain = new Sprite( new List<Texture2D>() { World.TextureAssets[ "sky" ] } );
            // terrain speed
            TerrainSpeed = 0.5f;
        }
        /// <summary>
        /// Create a new Sky
        /// </summary>
        /// <param name="World">Gameworld that owns this entity</param>
        /// <param name="AssetName">Terrain Texture</param>
        public Sky( GameWorld World, string AssetName )
        {
            // World
            this.World = World;
            // terrain
            Terrain = new Sprite( new List<Texture2D>() { World.TextureAssets[ AssetName ] } );
            // Speed
            TerrainSpeed = 0.5f;
        }
        #endregion
    }
}