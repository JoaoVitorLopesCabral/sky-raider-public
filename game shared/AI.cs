﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

// Debug 
using System.IO;

namespace SkyRaider.Game
{
    /// <summary>
    /// Available AI responses to player interaction
    /// </summary>
    public enum ResponseEvent
    {
        /// <summary>
        /// Do nothing
        /// </summary>
        None,
        /// <summary>
        /// Go investigate if anything happens within it's influence area
        /// </summary>
        Investigate,
        /// <summary>
        /// Move away from event origin
        /// </summary>
        MoveAway,
    };
    /// <summary>
    /// AI Entities
    /// Used for AI Combat as well.
    /// </summary>
    public class CombatEntity : Entity
    {
        #region Movement Properties
        /// <summary>
        /// Movement speed (pixels/update)
        /// </summary>
        public float MovementSpeed { get; set; }
        #endregion

        #region Other
        /// <summary>
        /// Relative angles
        /// </summary>
        public Angle Angles { get; set; }
        #endregion

        #region Attack
        /// <summary>
        /// Attack speed
        /// </summary>
        public float AttackSpeed { get; set; }
        /// <summary>
        /// Unit weapon
        /// </summary>
        public WeaponType Weapon { get; set; }
        /// <summary>
        /// Last attack time
        /// </summary>
        public double LastAttack { get; set; }
        #endregion

        #region Attached Entities
        /// <summary>
        /// Bullets fired by this entity
        /// </summary>
        public List<Entity> Bullets { get; set; }
        /// <summary>
        /// Movement path
        /// </summary>
        public List<Point> Path { get; set; }
        #endregion

        #region Behavior
        /// <summary>
        /// Entity response action
        /// </summary>
        public ResponseEvent Response { get; set; }
        /// <summary>
        /// If running away, how far should it go?
        /// </summary>
        public int RunDistance { get; set; }
        /// <summary>
        /// Influence area range
        /// </summary>
        public int InfluenceRange { get; set; }
        /// <summary>
        /// Any other entity in this area will trigger an object too close alert.
        /// </summary>
        public Rectangle InfluenceArea
        {
            get
            {
                return new Rectangle( (int)Origin.X - InfluenceRange, (int)Origin.Y - InfluenceRange,
                    (int)Origin.X + InfluenceRange, (int)Origin.Y + InfluenceRange );
            }
        }
        /// <summary>
        /// In sleep mode, the entity will only gather information about it's influence area.
        /// But won't move or attack.
        /// </summary>
        public bool IsSleeping { get; set; }
        /// <summary>
        /// Gets/Sets if this unit can patrol
        /// </summary>
        public bool IsPatrolUnit { get; set; }
        /// <summary>
        /// Gets/Sets if this unit is moving (if it's patrolling this value is false)
        /// </summary>
        public bool IsMoving { get; set; }
        /// <summary>
        /// Patrolling target
        /// If response is different than NONE, it will override patrolling
        /// </summary>
        public Point PatrolTarget;
        /// <summary>
        /// Target entity
        /// </summary>
        public Entity Target { get; set; }
        /// <summary>
        /// Can the unit think?
        /// </summary>
        public bool CanThink { get; set; }
        /// <summary>
        /// Interval to call Think()
        /// </summary>
        public double ThinkTime { get; set; }
        /// <summary>
        /// Last think time
        /// </summary>
        public double LastThink { get; set; }
        #endregion

        #region Functions
        /// <summary>
        /// Update entity data
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update( GameTime gameTime )
        {
            // death animation
            if ( State == EntityState.Dead )
            {
                if ( deathAnimFrames > DeathAnim.getFrameCount() )
                {
                    // transfer entities
                    for ( int i = 0; i < Bullets.Count; i++ )
                    {
                        World.Entities.Add( Bullets[ i ] );
                    }
                    // entity dead and has finished running death animation, remove it
                    Kill();
                }

                if ( gameTime.TotalGameTime.TotalSeconds - LastUpdate >= DeathAnim.Speed )
                {
                    DeathAnim.SetNextFrame();
                    deathAnimFrames++;
                }

                // stop update code
                base.Update( gameTime );
                return;
            }

            // Collision 
            foreach ( Entity ent in World.Entities )
            {
                // by group
                if ( ent.Collision == CollisionGroup.NotSolid )
                    continue;
                if ( ent.Collision == CollisionGroup.Special )
                    continue;
                // itself
                if ( ent == this )
                    continue;

                // now, retrieve entity bounds
                if ( ent.GetBounds().Intersects( GetBounds() ) )
                {
                    // is entity invulnerable
                    if ( ent.State == EntityState.Invulnerable )
                        continue;
                    // kill each other
                    ent.Hit( this );
                    Hit( ent );
                }
            }

            // Think
            // can it really think?
            if ( CanThink )
            {
                if ( gameTime.TotalGameTime.TotalSeconds - LastThink > ThinkTime )
                {
                    // call think
                    Think();
                    // update time
                    LastThink = gameTime.TotalGameTime.TotalSeconds;
                }
            }

            // Sleeping
            if ( IsSleeping )
                return;

            // If awake, and patrolling is enabled
            if ( IsPatrolUnit )
            {
                // movement vector
                Vector2 moveTo = new Vector2();
                // check if the unit has reached the target point
                /*if ( Position.X <= PatrolTarget.X )
                    moveTo = SpawnPosition;
                else if ( Position.X >= SpawnPosition.X )
                    moveTo = Util.ToVector2( PatrolTarget );*/

                Position.X += ( ( moveTo.X - Position.X < 0 ) ? -MovementSpeed : +MovementSpeed );
            }

            // If moving (not to be confused with patrolling)
            if ( IsMoving )
            {
                // check if path is empty
                if ( Path.Count != 0 )
                {
                    // retrieve next point
                    Point dest = Path[ Path.Count - 1 ];
                    // move
                    Position.X += ( ( Origin.X - dest.X > 0 ) ? -MovementSpeed : +MovementSpeed );
                    // are we there already?
                    if ( Position.X == dest.X )
                        Path.Remove( dest );
                }
            }

            // bullet update
            for ( int i = 0; i < Bullets.Count; i++ )
            {
                Bullets[ i ].Update( gameTime );
            }

            // fire?
            if ( Weapon != WeaponType.None && Target != null && ( gameTime.TotalGameTime.TotalSeconds - LastAttack > AttackSpeed ) )
            {
                // perform attack
                Attack();
                // update last attack info
                LastAttack = gameTime.TotalGameTime.TotalSeconds;
            }
            
            // last update
            LastUpdate = gameTime.TotalGameTime.TotalSeconds;

            // Vertical movement
            base.Update( gameTime );
        }
        /// <summary>
        /// Remove this entity from the game
        /// </summary>
        public override void Kill()
        {
            World.AIUnits.Remove( this );
        }
        /// <summary>
        /// Behavior
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Think()
        {
            // check if previously acquired target is still within range
            if ( Target != null && Vector2.Distance( Origin, Target.Origin ) > InfluenceRange )
                Target = null;
            // Find player withing it's influece area
            foreach ( Player player in World.Players )
            {
                if ( InfluenceArea.Contains( Util.ToPoint( player.Position ) ) )
                {
                    Target = player;
                }
            }
        }
        /// <summary>
        /// Attack stuff
        /// </summary>
        public virtual void Attack()
        {
            // base attack mode
            // create bullet at origin
            // add bullet to bullets list
            Bullets.Add( new AACannonBullet( this ) );
        }
        /// <summary>
        /// Draws this entity
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw( SpriteBatch spriteBatch )
        {
            // no death sprite so far, so, ignore it
            base.Draw( spriteBatch );

            // bullets
            foreach ( Entity bullet in Bullets )
            {
                bullet.Draw( spriteBatch );
            }
        }
        /// <summary>
        /// Wakes the entity
        /// </summary>
        /// <param name="Activator"></param>
        public void Wake( Entity Activator )
        {
            // awake
            IsSleeping = false;

            // if has weapons, set target
            if ( Weapon != WeaponType.None )
                Target = Activator;
            // set angles
            Angles = new Angle(this);
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new entity for AI
        /// </summary>
        public CombatEntity()
        {
            // Health
            Health = 1;
            // No weapons
            Weapon = WeaponType.None;
            // Influence range
            InfluenceRange = 200;
            // Collision
            Collision = CollisionGroup.Solid;
            // Bullets
            Bullets = new List<Entity>();
            // Path
            Path = new List<Point>();
            // Think time
            ThinkTime = 2.0f;
            // Can it think?
            CanThink = true;
            // Movement speed
            MovementSpeed = 3.0f;
            // Run distance
            RunDistance = 500;
            // Attack
            AttackSpeed = 2.5f;
            // Angle
            Angles = new Angle( this );
            // Score value
            ScoreValue = 30;
        }
        #endregion
    }
}