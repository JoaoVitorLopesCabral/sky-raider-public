﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Enemy Boat
    /// </summary>
    public class Boat : CombatEntity
    {
        #region Properties
        
        #endregion

        #region Functions
        /// <summary>
        /// Draw the boat.
        /// Since this entity has no animation, except for facing sides, we gotta override the CombatEntity.Draw() function
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw( SpriteBatch spriteBatch )
        {
            // just check the distance from the original X position
            float dx = ( Position.X - ( ( Path.Count > 0 ) ? Path[ Path.Count - 1 ].X : 0 ) );
            // get image
            string textAsset = ( dx < 0 ) ? "boat_east" : "boat_west";
            // bullets
            foreach ( Entity bullet in Bullets )
            {
                bullet.Draw( spriteBatch );
            }
            // draw
            spriteBatch.Draw( World.TextureAssets[ textAsset ], Position, Color.White );
        }
        /// <summary>
        /// Attack logic
        /// </summary>
        public override void Attack()
        {
            // create bullet
            Bullets.Add( new AACannonBullet( this ) );
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Create boat at the given position
        /// </summary>
        /// <param name="Position"></param>
        public Boat( Vector2 Position, GameWorld GameWorld )
        {
            this.Position = Position;
            World = GameWorld;
            Sprite = new Sprite( new List<Texture2D>() { World.TextureAssets[ "boat_west" ], World.TextureAssets[ "boat_east" ] } );
            AttackSpeed = 2.5f;
            ScoreValue = 30;
            // New collision flag
            Collision = CollisionGroup.AI;
        }
        #endregion
    }
    /// <summary>
    /// A frigate!
    /// </summary>
    public class Frigate : CombatEntity
    {
        #region Properties
        /// <summary>
        /// Entity sprite facing direction
        /// </summary>
        public FacingDirection FacingDirection { get; set; }
        #endregion

        #region Attack Properties
        /// <summary>
        /// Cannon A origin
        /// </summary>
        public Point CannonAOrigin
        {
            get
            {
                // get by facing dir
                if ( FacingDirection == FacingDirection.East )
                    return new Point( (int)Position.X + 35, (int)Position.Y + 11 );
                else if ( FacingDirection == FacingDirection.West )
                    return new Point( (int)Position.X + 27, (int)Position.Y + 11 );
                // if not set return entity origin
                return Util.ToPoint( Origin );
            }
        }
        /// <summary>
        /// Cannob B Origin
        /// </summary>
        public Point CannonBOrigin
        {
            get
            {
                // get by facing dir
                if ( FacingDirection == FacingDirection.East )
                    return new Point( (int)Position.X + 35, (int)Position.Y + 28 );
                else if ( FacingDirection == FacingDirection.West )
                    return new Point( (int)Position.X + 27, (int)Position.Y + 28 );
                // if not set return entity origin
                return Util.ToPoint( Origin );
            }
        }
        #endregion

        #region Functions
        /// <summary>
        /// Draw
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw( SpriteBatch spriteBatch )
        {
            if ( State == EntityState.Dead )
            {
                spriteBatch.Draw( DeathAnim.getFrame(), Position, DeathAnim.RenderInfo.TintingColor );
            }
            else
            {
                // get facing direction
                float dx = ( Position.X - ( ( Path.Count > 0 ) ? Path[ Path.Count - 1 ].X : 0 ) );
                // asset
                string Asset = ( dx >= 0 ) ? "frigate_west" : "frigate_east";

                // bullets
                for ( int i = 0; i < Bullets.Count; i++ )
                {
                    Bullets[ i ].Draw( spriteBatch );
                }

                // Draw

                //double rotation = Math.Atan2( ( Position.Y + MovementDir.Y ) - Position.Y, ( Position.X + MovementDir.X ) - Position.X );  

                spriteBatch.Draw( World.TextureAssets[ Asset ], Position, Sprite.RenderInfo.TintingColor );
                /*spriteBatch.Draw( World.TextureAssets[ Asset ], Position, null, Sprite.Render.TintingColor,
                    0, Vector2.Zero, Sprite.Scale, SpriteEffects.None, 0 ); */
            }
        }
        /// <summary>
        /// Frigate attack
        /// </summary>
        public override void Attack()
        {
            // It uses 2 cannons
            Bullets.Add( new AACannonBullet( this, CannonAOrigin ) );
            Bullets.Add( new AACannonBullet( this, CannonBOrigin ) );
        }
        /// <summary>
        /// Initializes a new Frigate instance
        /// </summary>
        /// <param name="World"></param>
        public Frigate( GameWorld World )
        {
            // World
            this.World = World;
            // Attack
            AttackSpeed = 2.5f;
            // Sprite
            Sprite = new Sprite( new List<Texture2D>() { World.TextureAssets[ "frigate_west" ], World.TextureAssets[ "frigate_east" ] } );
            // death anim
            DeathAnim = new Sprite( World.AnimationAssets[ "explosion" ] );
            DeathAnim.Speed = 0.75f;
            DeathAnim.Scale = new Vector2( 5 );
            // Weapon
            Weapon = WeaponType.DoubleCannon;
            // Influence
            InfluenceRange = 130;
            // Think
            ThinkTime = 3.0;
        }
        #endregion
    }
    /// <summary>
    /// Sentry Turret (a machine gun flying on cloud =D )
    /// </summary>
    public class SentryTurret : CombatEntity
    {
        #region Properties
        #endregion

        #region Functions
        #endregion

        #region Constructor
        /// <summary>
        /// Create a new SentryTurret
        /// </summary>
        /// <param name="World"></param>
        public SentryTurret( GameWorld World )
        {
            // set world
            this.World = World;
            // load texture
            Sprite = new Sprite( new List<Texture2D>() { World.TextureAssets[ "turret" ] } );
            // Attack speed (faster than frigates)
            AttackSpeed = 1.0f;
            // death anim
            DeathAnim = new Sprite( World.AnimationAssets[ "explosion" ] );
            DeathAnim.Speed = 0.75f;
            DeathAnim.Scale = new Vector2( 5 );
            // weapon
            Weapon = WeaponType.Cannon;
        }
        #endregion
    }
}