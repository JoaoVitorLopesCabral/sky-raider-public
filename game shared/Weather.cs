﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Defines a cloud entity
    /// </summary>
    public class Cloud : EnviromentalEntity
    {
        #region Properties
        /// <summary>
        /// Entity Type
        /// </summary>
        public string Name { get { return "Cloud"; } }
        /// <summary>
        /// Sets if the entity should use a static sprite instead of an animation
        /// </summary>
        public bool IsAnimated { get; set; }
        /// <summary>
        /// Movement Direction vector. (bi-directional, can be affected by wind entity).
        /// Usage: Position += Direction.Normalize() * Speed;
        /// </summary>
        public Vector2 Direction;
        /// <summary>
        /// Speed multiplier
        /// </summary>
        public float Speed;
        /// <summary>
        /// If true, it will disturb player and other AI
        /// </summary>
        public bool IsStormCloud;
        /// <summary>
        /// Scalling vector
        /// </summary>
        public Vector2 Scale;
        #endregion

        #region Functions
        /// <summary>
        /// Entity update
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update( GameTime gameTime )
        {
            // if it is a storm cloud, then use collision
            if ( IsStormCloud )
            {
                // find player
                foreach ( Player player in World.Players )
                {
                    if ( GetBounds().Contains( Util.ToPoint( player.Position ) ) )
                    {
                        // storm effect
                    }
                }
                // find other entities
                foreach ( Entity ent in World.Entities )
                {
                    // detect collision group
                    if ( ent.Collision == CollisionGroup.Solid )
                    {
                        if ( GetBounds().Contains( Util.ToPoint( ent.Position ) ) )
                        {
                            // storm effect
                        }
                    }
                }
            }

            // is cloud animated
            if ( IsAnimated )
            {
                animTotalTime += LastUpdate;

                if ( animTotalTime > Sprite.Speed )
                {
                    Sprite.SetNextFrame();
                    Sprite.SetFrame( Sprite.getFrameIndex() % Sprite.getFrameCount() );
                    animTotalTime -= Sprite.Speed;
                }
            }
            // cloud movement
            // Normalize Direction
            Direction.Normalize();
            // move
            Position += Direction * Speed;

            // last update
            LastUpdate = gameTime.TotalGameTime.TotalMilliseconds;
        }
        /// <summary>
        /// Drawing
        /// </summary>
        /// <param name="spriteBatch"></param>
        public override void Draw( SpriteBatch spriteBatch )
        {
            // if animated
            if ( IsAnimated )
                base.Draw( spriteBatch );
            else
                spriteBatch.Draw( Sprite.getFrame(), Position, null, Sprite.RenderInfo.TintingColor * Sprite.RenderInfo.Alpha,
                    Sprite.Rotation, Vector2.Zero, Scale, SpriteEffects.None, 0 );
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new Cloud Instance. ( using default values for scale and texture )
        /// </summary>
        /// <param name="World"></param>
        /// <param name="Direction"></param>
        /// <param name="Position"></param>
        public Cloud( GameWorld World, Vector2 Direction, Vector2 Position )
        {
            // World
            this.World = World;
            // direction
            this.Direction = Direction;
            // Position
            this.Position = Position;
            // Texture - assume static
            IsAnimated = false;
            // Assign texture
            Sprite = new Sprite( new List<Texture2D>() { World.TextureAssets[ "cloud_default" ] } );
            // set as 90% opaque
            Sprite.RenderInfo.Alpha = 0.9f;
            // scale
            Scale = new Vector2( 1 );
            // Speed
            Speed = 0.5f;
        }
        #endregion
    }
    /// <summary>
    /// Controls the weather
    /// </summary>
    public class Weather
    {
        #region Properties and Attached Entities
        /// <summary>
        /// Clouds below player layer
        /// </summary>
        public List<Entity> bottomClouds { get; set; }
        /// <summary>
        /// Clouds above player layer
        /// </summary>
        public List<Entity> topClouds { get; set; }
        /// <summary>
        /// Wind Entities
        /// </summary>
        public List<Entity> WindEntities { get; set; }
        /// <summary>
        /// Last time Think was called
        /// </summary>
        private double lastThink;
        /// <summary>
        /// Owner
        /// </summary>
        public GameWorld World { get; set; }
        #endregion

        #region Functions
        /// <summary>
        /// Update attached entities
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update( GameTime gameTime )
        {
            // bottom clouds
            for ( int i = 0; i < bottomClouds.Count; i++ )
            {
                bottomClouds[ i ].Update( gameTime );
            }
            // top clouds
            for ( int i = 0; i < topClouds.Count; i++ )
            {
                topClouds[ i ].Update( gameTime );
            }

            // update wind stuff
            for ( int i = 0; i < WindEntities.Count; i++ )
            {
                WindEntities[ i ].Update( gameTime );
            }

            // think
            if ( gameTime.TotalGameTime.TotalSeconds - lastThink > 10 )
            {
                // call
                Think( gameTime );
                // store last think val
                lastThink = gameTime.TotalGameTime.TotalSeconds;
            }
        }
        /// <summary>
        /// Draw attached entities
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw( SpriteBatch spriteBatch )
        {
            // bottom clouds
            for ( int i = 0; i < bottomClouds.Count; i++ )
            {
                bottomClouds[ i ].Draw( spriteBatch );
            }
            // top clouds will be rendered within the main code file
        }
        /// <summary>
        /// Remove the given entity from the weather system
        /// </summary>
        /// <param name="entity"></param>
        public void Remove( Entity entity )
        {
            if ( bottomClouds.Contains( entity ) )
                bottomClouds.Remove( entity );
            else if ( topClouds.Contains( entity ) )
                topClouds.Remove( entity );
        }
        /// <summary>
        /// Control weather changes
        /// </summary>
        /// <param name="gameTime"></param>
        public void Think( GameTime gameTime )
        {
            // Create odds
            int odds = new Random( World.Players[ 0 ].Score ).Next( 0, 101 );
            // call events if the value is within their chance range
            if ( odds >= 0 && odds < 101 )
            {
                // Clear sky, only may change the wind
                int subOdds = new Random( World.Players[ 0 ].Score ).Next( 0, 101 );
                //if ( subOdds > 70 )
                //{
                    // change wind
                    foreach ( Wind windEnt in WindEntities )
                    {
                        //windEnt.Direction = new Vector2( new Random( World.Players[ 0 ].Score ).Next( -512, 512 ),
                        //    new Random( World.Players[ 0 ].Score ).Next( -512, 512 ) );
                        windEnt.Direction = new Vector2( 0, 512 );
                        // allow thinking
                        windEnt.AllowThinking();
                    }
                //}
            }
            else if ( odds > 70 && odds <= 75 )
            {
                // start to draw rain clouds
                for ( int i = 0; i < bottomClouds.Count; i++ )
                {
                    bottomClouds[ i ].Sprite.RenderInfo.TintingColor = Color.DarkGray;
                }
                for ( int i = 0; i < topClouds.Count; i++ )
                {
                    topClouds[ i ].Sprite.RenderInfo.TintingColor = Color.DarkGray;
                }
            }
        }
        #endregion

        #region Constructor
        public Weather( GameWorld World )
        {
            // Init entity lists
            bottomClouds = new List<Entity>();
            topClouds = new List<Entity>();
            // no more than 3 entities 
            WindEntities = new List<Entity>()
            {
                new Wind(WindLayer.Bottom, Vector2.Zero, 1, World ),
                new Wind(WindLayer.Top, Vector2.Zero, 1, World),
                new Wind(WindLayer.Global, Vector2.Zero, 1, World),
            };
            // Owner
            this.World = World;
        }
        #endregion
    }
    /// <summary>
    /// Wind depth flags
    /// </summary>
    public enum WindLayer
    {
        /// <summary>
        /// Affects the top layer only (including players and enemy AI)
        /// </summary>
        Top,
        /// <summary>
        /// Apply wind effects only in the bottommost layer
        /// </summary>
        Bottom,
        /// <summary>
        /// Wind everywhere!
        /// </summary>
        Global,
    }

    #region Wind Events
    /// <summary>
    /// WindChanged event arguments
    /// </summary>
    public class WindChangedEventArgs : EventArgs
    {
        #region Elements

        

        #endregion
    }

    #endregion

    /// <summary>
    /// Wind effect entity
    /// </summary>
    public class Wind : Entity
    {
        #region Properties
        /// <summary>
        /// wind direction
        /// </summary>
        private Vector2 _direction;
        /// <summary>
        /// Wind direction
        /// </summary>
        public Vector2 Direction
        {
            get
            {
                return _direction;
            }
            set
            {
                // raise OnDirectionChanged event

                // set val
                _direction = value;
            }
        }
        /// <summary>
        /// Wind speed
        /// </summary>
        private float Speed;
        /// <summary>
        /// Where to apply wind effects
        /// </summary>
        public WindLayer Layer { get; set; }
        #endregion

        #region Function
        public override void Update( GameTime gameTime )
        {
            // call think to apply it's effects
            if ( gameTime.TotalGameTime.TotalSeconds - LastThinkTime > 1 && ShouldThink )
            {
                Think();

                LastThinkTime = gameTime.TotalGameTime.TotalSeconds;

                // once called, stop thinking
                ShouldThink = false;
            }

            base.Update( gameTime );
        }
        /// <summary>
        /// Wind behaviour
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Think()
        {
            // find cloud entities to change their direction
            // Bottom layer
            if ( Layer == WindLayer.Bottom )
            {
                for ( int i = 0; i < World.Weather.bottomClouds.Count; i++ )
                {
                    World.Weather.bottomClouds[ i ].Direction = Direction;
                    ( (Cloud)World.Weather.bottomClouds[ i ] ).Speed = Speed;
                }
            }
            else if ( Layer == WindLayer.Top )
            {
                // top
                for ( int i = 0; i < World.Weather.topClouds.Count; i++ )
                {
                    World.Weather.topClouds[ i ].Direction = Direction;
                    ( (Cloud)World.Weather.topClouds[ i ] ).Speed = Speed;
                }
            }
            else if ( Layer == WindLayer.Global )
            {
                // bottom
                for ( int i = 0; i < World.Weather.bottomClouds.Count; i++ )
                {
                    World.Weather.bottomClouds[ i ].Direction = Direction;
                    ( (Cloud)World.Weather.bottomClouds[ i ] ).Speed = Speed;
                }
                // top
                for ( int i = 0; i < World.Weather.topClouds.Count; i++ )
                {
                    World.Weather.topClouds[ i ].Direction = Direction;
                    ( (Cloud)World.Weather.topClouds[ i ] ).Speed = Speed;
                }
            }
        }
        /// <summary>
        /// Assign a new direction
        /// </summary>
        /// <param name="Direction"></param>
        public void SetDirection( Vector2 Direction )
        {
            this.Direction = Direction;
        }
        /// <summary>
        /// Set ShouldThink value to true
        /// </summary>
        public void AllowThinking()
        {
            ShouldThink = true;          
        }
        #endregion

        #region Constructor
        /// <summary>
        /// New wind entity, with default parameters
        /// </summary>
        public Wind() { ShouldThink = false; }
        /// <summary>
        /// New wind entity.
        /// </summary>
        /// <param name="Layer">Entity layer to be affected</param>
        /// <param name="Direction">Wind direction</param>
        /// <param name="Speed">Speed</param>
        public Wind( WindLayer Layer, Vector2 Direction, float Speed, GameWorld World )
        {
            // always start disabled
            ShouldThink = false;
            // set-up entity
            this.Layer = Layer;
            this.Direction = Direction;
            this.Speed = Speed;
            // world
            this.World = World;
        }
        #endregion
    }
}