﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    /// <summary>
    /// Defines the game world!
    /// </summary>
    public class GameWorld
    {
        #region Data Access
        /// <summary>
        /// Textures available for use
        /// </summary>
        public Dictionary<string, Texture2D> TextureAssets { get; set; }
        /// <summary>
        /// Available animations
        /// </summary>
        public Dictionary<string, List<Texture2D>> AnimationAssets { get; set; }
        #endregion

        #region Client Info
        /// <summary>
        /// Some client info
        /// </summary>
        public Client ClientInfo { get; set; }
        /// <summary>
        /// Current Frame rate
        /// </summary>
        int FramesPerSecond { get; set; }
        /// <summary>
        /// Frame rate update time
        /// </summary>
        double LastFrameUpdate { get; set; }
        /// <summary>
        /// Frame count
        /// </summary>
        int FrameCount { get; set; }
        #endregion

        #region Entity Access
        /// <summary>
        /// GameRules associated with this world
        /// </summary>
        public GameRules Rules { get; set; }
        /// <summary>
        /// Available Entities
        /// </summary>
        public List<Entity> Entities { get; set; }
        /// <summary>
        /// AI Units
        /// </summary>
        public List<CombatEntity> AIUnits { get; set; }
        /// <summary>
        /// Players connected
        /// </summary>
        public List<Player> Players { get; set; }
        /// <summary>
        /// Available sky sets
        /// </summary>
        public List<Sky> SkyList { get; set; }
        /// <summary>
        /// Weather System
        /// </summary>
        public Weather Weather { get; set; }
        /// <summary>
        /// Font
        /// </summary>
        public SpriteFont Font { get; set; }
        /// <summary>
        /// Usable fonts
        /// </summary>
        public Dictionary<string, SpriteFont> FontAssets { get; set; }
        /// <summary>
        /// Stores info related to graphics device
        /// </summary>
        private GraphicsDeviceManager graphics;
        /// <summary>
        /// Current sky that is being drawn
        /// </summary>
        private int currentLevel = 0;
        /// <summary>
        /// last sky item to leave the screen
        /// used for clearing purposes
        /// </summary>
        private int previousLevel = -1;
        #endregion

        #region Enemy Spawining Properties
        /// <summary>
        /// Difficulty level
        /// </summary>
        private double difficultyLevel
        {
            get
            {
                // difficulty increases every 500 points
                // but multipliers every 2 levels
                //int level = ( ( (Player)Players[ 0 ] ).TotalScore < 500 ) ? 1 : ( (Player)Players[ 0 ] ).TotalScore / 500;
                
                //float multiplier = 1 + 0.1f * ( ( level / 2 < 1 ) ? 0 : (int)level / 2 );
                float multiplier = 1 + 0.1f * ( ( ( Players[ 0 ].Level / 2 ) < 1 ) ? 0 : (int)( Players[ 0 ].Level / 2 ) );
 
                // change score multiplier
                //Rules.ScoreModifier = multiplier;

                //return level * multiplier;
                return Players[ 0 ].Level * multiplier;
            }
        }

        // -- New stuff May 29 2014
        /// <summary>
        /// Total amount of enemy entities to spawn
        /// </summary>
        public int EnemySpawnQuantity { get; set; }
        /// <summary>
        /// Time between spawning more enemies
        /// </summary>
        double EnemySpawnTime { get; set; }
        /// <summary>
        /// Last time an enemy unit was spawned
        /// </summary>
        double LastEnemySpawnTime { get; set; }
        /// <summary>
        /// Total count of fuel depots to spawn
        /// </summary>
        public int FuelSpawnQuantity { get; set; }
        /// <summary>
        /// Entity used to spawn new enemies
        /// </summary>
        Entity SpawnAreaEntity { get; set; }
        #endregion

        #region DEBUG
        #if DEBUG
        public int AICount = 0;
        #endif
        #endregion

        #region Functions
        /// <summary>
        /// Creates a new player!
        /// </summary>
        /// <returns></returns>
        public Player CreateNewPlayer()
        {
            // Create temp player object
            Player newPlayer = new Player( this );

            // Load initial spawn point
            newPlayer.Position = Entities[ 0 ].Position;
            // return new entity
            return newPlayer;
        }
        /// <summary>
        /// Handles game update
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update( GameTime gameTime )
        {       
            // fps
            if ( gameTime.TotalGameTime.TotalSeconds - LastFrameUpdate >= 1 )
            {
                FramesPerSecond = FrameCount;
                LastFrameUpdate = gameTime.TotalGameTime.TotalSeconds;
                FrameCount = 0;
            }
            else
            {
                FrameCount++;
            }
            // is player using the menu?
            if ( Rules.State == GameState.OnMenu )
                return;
            // players 
            for ( int i = 0; i < Players.Count; i++ )
                Players[ i ].Update( gameTime );
            // Update if running
            if ( Rules.State == GameState.GameRunning )
            {
                // terrain and sky
                for ( int i = 0; i < SkyList.Count; i++ )
                    SkyList[ i ].Update( gameTime );
                // check sky pos
                // -- using 3 Sky Objects only
                
                // -- check if the current sky object is beyond screen pos
                /*if ( SkyList[ 0 ].TerrainPosition.Y > ClientInfo.ScreenHeight )
                {
                    // -- create new sky
                    CreateSky( 1 );
                    // -- remove first item
                    SkyList.RemoveAt( 0 );
                }*/
                /*if ( SkyList[ currentLevel ].TerrainPosition.Y > ClientInfo.ScreenHeight )
                {
                    // Create a new sky
                    CreateSky( 1 );
                    // mark as previous level
                    previousLevel++;
                    // move to next level
                    currentLevel++;
                }
                // check if the previous sky is off screen already
                if ( previousLevel >= 0 )
                {
                    if ( SkyList[ previousLevel ].TerrainPosition.Y > ClientInfo.ScreenHeight )
                    {
                        // remove item from list
                        SkyList.RemoveAt( previousLevel );
                    }
                }*/
                // entities
                for ( int i = 0; i < Entities.Count; i++ )
                    Entities[ i ].Update( gameTime );
                // AI
                for ( int i = 0; i < AIUnits.Count; i++ )
                    AIUnits[ i ].Update( gameTime );
                // Weather
                //Weather.Update( gameTime );
                
                // reset enemy count every 10 levels
                /*if ( Players[ 0 ].Level % Rules.LevelToResetEnemyQuantity == 0 )
                {
                    EnemySpawnQuantity = Rules.BaseEnemyQuantity;
                    // increase enemy level
                    Rules.EnemyLevel++;
                }*/
                // increase enemies based on difficulty
                EnemySpawnQuantity = (int)( Rules.BaseEnemyQuantity * ( 1 + ( ( difficultyLevel * Rules.EnemyIncrementFactor ) / 100 ) ) );
                // decrease fuel depots
                FuelSpawnQuantity = ( Rules.BaseFuelQuantity * ( 1 + ( ( difficultyLevel * Rules.FuelIncrementFactor ) / 100 ) ) ) < 1 ? 1 : (int)( Rules.BaseFuelQuantity * ( 1 + ( ( difficultyLevel * Rules.FuelIncrementFactor ) / 100 ) ) );

                // spawn enemies
                SpawnAreaEntity.Update( gameTime );
             }
        }
        /// <summary>
        /// Draw the game 
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw( SpriteBatch spriteBatch )
        {
            // On game over
            if ( Rules.State == GameState.GameOver )
            {
                // Draw gameover image
                spriteBatch.Draw( TextureAssets["game_over"], new Vector2( ( graphics.PreferredBackBufferWidth / 2 ) - 80,
                    ( graphics.PreferredBackBufferHeight / 2 ) - 20 ), Color.White );
            }
            else
            {
                // Game is running
                // sky
                for ( int i = 0; i < SkyList.Count; i++ )
                    SkyList[ i ].Draw( spriteBatch );
                // weather
                //Weather.Draw( spriteBatch );
                // entities
                for ( int i = 0; i < Entities.Count; i++ )
                    Entities[ i ].Draw( spriteBatch );
                // new ai
                for ( int i = 0; i < AIUnits.Count; i++ )
                    AIUnits[ i ].Draw( spriteBatch );
                // players
                for ( int i = 0; i < Players.Count; i++ )
                    Players[ i ].Draw( spriteBatch );
                // top layer clouds
                /*for ( int i = 0; i < Weather.topClouds.Count; i++ )
                    Weather.topClouds[ i ].Draw( spriteBatch );*/

                

                // debug
                /*spriteBatch.DrawString( Font, "DEBUG FLAGS: " + Players[ 0 ].dFlags.ToString(), new Vector2( 5, 20 ), Color.Yellow );
                spriteBatch.DrawString( Font, "Difficulty: " + difficultyLevel.ToString(), new Vector2( 5, 35 ), Color.White );
                spriteBatch.DrawString( Font, "Score Multiplier: " + Rules.ScoreModifier.ToString(), new Vector2( 5, 50 ), Color.White );
                spriteBatch.DrawString( Font, "Fuel Depots: " + FuelSpawnQuantity.ToString(), new Vector2( 5, 65 ), Color.Red );
                spriteBatch.DrawString( Font, "Enemies: " + EnemySpawnQuantity.ToString(), new Vector2( 5, 80 ), Color.Red );

                // FPS
                spriteBatch.DrawString( Font, "FPS: " + FramesPerSecond.ToString(), ClientInfo.GetRelativePosition(93,1), Color.White );*/
                
                // --- DEBUG
                if ( Environment.GetCommandLineArgs()[0] == "-debug" )
                    spriteBatch.DrawString( Font, "SKY COUNT: " + SkyList.Count.ToString(), new Vector2( 5, 20 ), Color.White );
                // --- DEBUG
                // Hud

                // score 
                spriteBatch.DrawString( FontAssets[ "hud_font" ], Players[ 0 ].Score.ToString(), ClientInfo.GetRelativePosition( 2.5f, 85 ), Color.Black );

                // health
                spriteBatch.Draw( TextureAssets[ "hud_health" ], ClientInfo.GetRelativePosition(2.5f,90), null, Color.White, 0, Vector2.Zero,
                    new Vector2( Players[ 0 ].Health * 100, 1 ), SpriteEffects.None, 0 );
                spriteBatch.Draw( TextureAssets[ "hud_panel" ], ClientInfo.GetRelativePosition(2.51f,90), Color.White );
                // - life symbol
                spriteBatch.Draw( TextureAssets[ "hud_lives" ], ClientInfo.GetRelativePosition( 12, 90 ), Color.White );
                spriteBatch.DrawString( FontAssets[ "hud_font" ], Players[ 0 ].Lives.ToString(), ClientInfo.GetRelativePosition( 14.5f, 89 ), Color.Black );
                // fuel
                spriteBatch.Draw( TextureAssets[ "hud_fuel" ], ClientInfo.GetRelativePosition(2.5f,93), null, Color.White, 0, Vector2.Zero,
                    new Vector2( Players[ 0 ].Fuel * 100, 1 ), SpriteEffects.None, 0 );
                spriteBatch.Draw( TextureAssets[ "hud_panel" ], ClientInfo.GetRelativePosition( 2.51f, 93 ), Color.White );
                // experience
                // center panels
                Vector2 panelCenter = new Vector2( ClientInfo.CenterElement( TextureAssets[ "hud_panel" ].Width ), ClientInfo.GetRelativePosition( 0, 93.5f ).Y );
                spriteBatch.Draw( TextureAssets[ "hud_xp" ], panelCenter, null, Color.White, 0, Vector2.Zero,
                    new Vector2( Players[ 0 ].GetLevelProgress() * 200, 0.5f ), SpriteEffects.None, 0 );
                spriteBatch.Draw( TextureAssets[ "hud_panel" ], panelCenter, null, Color.White, 0, Vector2.Zero,
                    new Vector2( 2, 0.5f ), SpriteEffects.None, 0 );
                spriteBatch.DrawString( FontAssets[ "hud_font" ], Players[ 0 ].Level.ToString(), ClientInfo.GetRelativePosition(53,95), Color.Black );


                // if game is paused
                if ( Rules.State == GameState.Paused )
                    spriteBatch.Draw( TextureAssets[ "game_paused" ], new Vector2( ( graphics.PreferredBackBufferWidth / 2 ) - 80,
                        ( graphics.PreferredBackBufferHeight / 2 ) - 20 ), Color.White );
            }
            
        }
        /// <summary>
        /// Load game data
        /// </summary>
        /// <param name="Content"></param>
        public void Precache( ContentManager Content )
        {
            // Other graphical assets
            TextureAssets.Add( "game_paused", Content.Load<Texture2D>( "game_paused" ) );
            TextureAssets.Add( "game_over", Content.Load<Texture2D>( "game_over" ) );

            // Fonts
            FontAssets.Add( "hud_font", Content.Load<SpriteFont>( "Consolas" ) );
            Font = Content.Load<SpriteFont>( "Font" );

            // HUD
            TextureAssets.Add( "hud_panel", Content.Load<Texture2D>( "hud_panel" ) );
            TextureAssets.Add( "hud_fuel", Content.Load<Texture2D>( "hud_fuel" ) );
            TextureAssets.Add( "hud_health", Content.Load<Texture2D>( "hud_health" ) );
            TextureAssets.Add( "hud_lives", Content.Load<Texture2D>( "hud_lives" ) );
            TextureAssets.Add( "hud_xp", Content.Load<Texture2D>( "hud_xp" ) );

            // Sky
            TextureAssets.Add( "sky", Content.Load<Texture2D>( "sky" ) );

            // Textures
            TextureAssets.Add( "bullet_default", Content.Load<Texture2D>( "bullet_default" ) );
            TextureAssets.Add( "bullet_aa", Content.Load<Texture2D>( "bullet_aa" ) );
            TextureAssets.Add( "plane_engine_0", Content.Load<Texture2D>( "plane_engine_0" ) );
            TextureAssets.Add( "plane_engine_1", Content.Load<Texture2D>( "plane_engine_1" ) );
            TextureAssets.Add( "plane_engine_2", Content.Load<Texture2D>( "plane_engine_2" ) );
            TextureAssets.Add( "plane_engine_3", Content.Load<Texture2D>( "plane_engine_3" ) );
            TextureAssets.Add( "plane_engine_4", Content.Load<Texture2D>( "plane_engine_4" ) );
            TextureAssets.Add( "fuel", Content.Load<Texture2D>( "fuel" ) );
            TextureAssets.Add( "sky_bottom_layer", Content.Load<Texture2D>( "sky_bottom_layer" ) );
            TextureAssets.Add( "player_bounds", Content.Load<Texture2D>( "player_bounds" ) );
            TextureAssets.Add( "sky_background", Content.Load<Texture2D>( "sky_background" ) );
            TextureAssets.Add( "cloud_default", Content.Load<Texture2D>( "cloud_default" ) );
            TextureAssets.Add( "frigate_west", Content.Load<Texture2D>( "frigate_west" ) );
            TextureAssets.Add( "frigate_east", Content.Load<Texture2D>( "frigate_east" ) );
            TextureAssets.Add( "turret", Content.Load<Texture2D>( "sentry_tower" ) );
            TextureAssets.Add( "fuel_depot", Content.Load<Texture2D>( "fuel_depot" ) );
            TextureAssets.Add( "fuel_depot_1", Content.Load<Texture2D>( "fuel_depot_1" ) );
            TextureAssets.Add( "fuel_depot_2", Content.Load<Texture2D>( "fuel_depot_2" ) );
            TextureAssets.Add( "fuel_depot_3", Content.Load<Texture2D>( "fuel_depot_3" ) );
            TextureAssets.Add( "fuel_depot_4", Content.Load<Texture2D>( "fuel_depot_4" ) );
            // Effects
            TextureAssets.Add( "explosion_0", Content.Load<Texture2D>( "explosion_0" ) );
            TextureAssets.Add( "explosion_1", Content.Load<Texture2D>( "explosion_1" ) );
            TextureAssets.Add( "explosion_2", Content.Load<Texture2D>( "explosion_2" ) );
            TextureAssets.Add( "explosion_3", Content.Load<Texture2D>( "explosion_3" ) );
            TextureAssets.Add( "explosion_4", Content.Load<Texture2D>( "explosion_4" ) );
            TextureAssets.Add( "explosion_5", Content.Load<Texture2D>( "explosion_5" ) );
            TextureAssets.Add( "blast", Content.Load<Texture2D>( "blast" ) );
            TextureAssets.Add( "blast_0", Content.Load<Texture2D>( "blast_0" ) );
            TextureAssets.Add( "blast_1", Content.Load<Texture2D>( "blast_1" ) );
            TextureAssets.Add( "blast_2", Content.Load<Texture2D>( "blast_2" ) );
            TextureAssets.Add( "blast_3", Content.Load<Texture2D>( "blast_3" ) );

            // Player
            TextureAssets.Add( "player_idle", Content.Load<Texture2D>( "plane_idle" ) );
            TextureAssets.Add( "player_left", Content.Load<Texture2D>( "plane_left" ) );
            TextureAssets.Add( "player_right", Content.Load<Texture2D>( "plane_right" ) );
            TextureAssets.Add( "player_bullet", Content.Load<Texture2D>( "plane_gunfire" ) );

            // Animations
            AnimationAssets.Add( "plane_engine", new List<Texture2D>()
            {
                TextureAssets["plane_engine_0"],
                TextureAssets["plane_engine_1"],
                TextureAssets["plane_engine_2"],
                TextureAssets["plane_engine_3"],
                TextureAssets["plane_engine_4"],
            } );

            AnimationAssets.Add( "explosion", new List<Texture2D>()
            {
                TextureAssets["explosion_0"],
                TextureAssets["explosion_1"],
                TextureAssets["explosion_2"],
                TextureAssets["explosion_3"],
                TextureAssets["explosion_4"],
                TextureAssets["explosion_5"],
            } );

            AnimationAssets.Add( "blast", new List<Texture2D>()
            {
                TextureAssets["blast_0"],
                TextureAssets["blast_1"],
                TextureAssets["blast_2"],
                TextureAssets["blast_3"],
            } );

            AnimationAssets.Add( "fuel_depot", new List<Texture2D>()
            {
                TextureAssets["fuel_depot"],
                TextureAssets["fuel_depot_1"],
                TextureAssets["fuel_depot_2"],
                TextureAssets["fuel_depot_3"],
                TextureAssets["fuel_depot_4"],
            } );

            // Create spawn point
            Entities.Add( new PlayerSpawnPoint( new Vector2( graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight - TextureAssets[ "player_idle" ].Height ), this ) );

            // setup spawn area
            SpawnAreaEntity = new UnitSpawnPoint( new Rectangle( 25, -ClientInfo.ScreenHeight, ClientInfo.ScreenWidth - TextureAssets[ "player_idle" ].Width, 600 ), this ); 

            // Create the map
            CreateSky( 1 );

            // Initialize entities
            Players.Add( CreateNewPlayer() );
        }
        /// <summary>
        /// Creates and adds a sky to list
        /// </summary>
        /// <param name="Count">Amount of sky units to create</param>
        public void CreateSky( int Count )
        {
            // create and attach
            for ( int i = 0; i < Count; i++ )
            {
                // temp object
                Sky sky = new Sky( this, "sky_background" );
                // set-up position
                sky.TerrainPosition = new Vector2( 0, ( -ClientInfo.ScreenHeight * i - ( ( SkyList.Count == 0 ) ? 0 : Math.Abs( SkyList[ SkyList.Count - 1 ].TerrainPosition.Y ) ) ) );
                // add to the list
                SkyList.Add( sky );
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// New Game World
        /// </summary>
        public GameWorld( GameRules Rules, GraphicsDeviceManager graphics )
        {
            // Player list
            Players = new List<Player>( Rules.MaxPlayers );
            // Entity list
            Entities = new List<Entity>();
            // Assets
            TextureAssets = new Dictionary<string, Texture2D>();
            AnimationAssets = new Dictionary<string, List<Texture2D>>();
            FontAssets = new Dictionary<string, SpriteFont>();
            // AI list
            AIUnits = new List<CombatEntity>();
            // Rules
            this.Rules = Rules;
            // sky
            SkyList = new List<Sky>();
            // Set state to waiting
            this.Rules.SetState( GameState.Waiting );
            // World created, time to set-up a spawn point

            // weather
            //Weather = new Weather(this);
            // graphics device
            this.graphics = graphics;
            // client info
            ClientInfo = new Client( graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight );
            
        }

        #endregion
    }
}