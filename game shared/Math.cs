﻿using System;
using System.Collections.Generic;
// -- XNA
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace SkyRaider.Game
{
    public static partial class Util
    {
        #region Math stuff
        /// <summary>
        /// Gets the distance between 2 points
        /// Based:  multiply-with-carry method invented by George Marsaglia
        /// </summary>
        /// <param name="A">Point A</param>
        /// <param name="B">Point B</param>
        /// <returns></returns>
        public static double Distance( Point A, Point B )
        {
            return Math.Sqrt( Math.Pow( A.X - B.X, 2 ) + Math.Pow( A.Y - B.Y, 2 ) );
        }
        #endregion

        #region Random
        /// <summary>
        /// Random class instance
        /// </summary>
        public static Random Random { get; set; }
        /// <summary>
        /// Generates a random integer
        /// </summary>
        /// <param name="seed1"></param>
        /// <param name="seed2"></param>
        /// <returns></returns>
        public static int RandomInt( int m_w, int m_z )
        {
            m_z = 36969 * ( m_z & 65535 ) + ( m_z >> 16 );
            m_w = 18000 * ( m_w & 65535 ) + ( m_w >> 16 );

            return ( m_z << 16 ) + m_w;
        }
        #endregion
    }
    /// <summary>
    /// Contains several angles related to the entity owner and its target
    /// </summary>
    public class Angle
    {
        #region Properties
        /// <summary>
        /// Owner
        /// </summary>
        public CombatEntity Owner { get; set; }
        /// <summary>
        /// Target
        /// </summary>
        public Entity Target { get; set; }
        #endregion

        #region Functions
        /// <summary>
        /// Gets the relative angle between Owner and Target
        /// </summary>
        /// <returns></returns>
        public double GetRelativeAngle()
        {
            // get dir vector
            /*Vector2 dir;
            if ( Target.Position.X > Owner.Position.X )
                dir = new Vector2( Target.Position.X - Owner.Position.X, Owner.Position.Y );
            else
                dir = new Vector2( Owner.Position.X - Target.Position.X, Owner.Position.Y );
            // get horizontal vector
            Vector2 h;
            if ( Target.Position.X < Owner.Position.X )
                h = new Vector2( Target.Position.X - Owner.Position.X, Owner.Position.Y );
            else
                h = ( new Vector2( Owner.Position.X - Target.Position.X, Owner.Position.Y ) ) * -1;
            // normalize vectors
            dir.Normalize();
            h.Normalize();
            // get angle
            return Vector2.Dot( dir, h );*/

            // let's use pithagoras for it
            float dx = Owner.Origin.X - Owner.Target.Origin.X;
            float dy = Owner.Target.Origin.Y - Owner.Origin.Y;
            double d = Vector2.Distance( Owner.Origin, Owner.Target.Origin );//Math.Sqrt( Math.Pow( dx, 2 ) + Math.Pow( dy, 2 ) );
            double betaSin = dy / d;
            // check X dif
            if ( dx < 0 )
                return Math.Asin( betaSin );

            return 1 + Math.Asin( betaSin );

            /*
             *  THE PROBLEM IS THE TEXTURE!!!!!
             *  USE A 1x1 TEXTURE.
             * 
             *  READ: http://www.xnawiki.com/index.php?title=Drawing_2D_lines_without_using_primitives
             * 
             * 
             * */

        }
        /// <summary>
        /// Gets a unitary vector pointed to the target
        /// </summary>
        /// <returns></returns>
        public Vector2 GetDirectionVector()
        {
            // get dir
            Vector2 dir =  Owner.Target.Origin - Owner.Origin;
            // normalize
            dir.Normalize();
            // return
            return dir;
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Start a new Angle instance
        /// </summary>
        /// <param name="Owner"></param>
        public Angle( CombatEntity Owner )
        {
            // assign owner
            this.Owner = Owner;
            // target
            Target = Owner.Target;
        }
        #endregion
    }
}